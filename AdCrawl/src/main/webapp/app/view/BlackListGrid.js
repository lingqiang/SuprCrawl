var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout : {
		overflowHandler : 'Menu'
	},
	items : [ {
		id:'searType',
		xtype : 'combo',
		name : 'searType',
		editable:false,
		fieldLabel : '类型',
		labelWidth : 40,
		width : 180,
		emptyText : '==请选择类型==',
		store : [['1', '城市'],['2', '用户']],
		listeners:{
			'blur':function(val){
				search();
			}
		}
	},{
		id:'content',
		xtype : 'textfield',
		name : 'content',
		fieldLabel : '内容关键字',
		labelWidth : 80,
		width : 250,
		emptyText : '===请填写内容关键字===',
		listeners:{
			'blur':function(){
				search();
			}
		}
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("content").setValue("");
			Ext.getCmp("searType").setValue("");
			search();
		}
	},{
		text : '添加',
		handler : function() {
			addBlackList();
		}
	} ]
});

function search(){
	var content = Ext.getCmp("content").getValue();
	var type = Ext.getCmp("searType").getValue();
	
	var blackListGrid = Ext.getCmp("blackListGrid").getStore();
	blackListGrid.load({
		params : {
			content : encodeURIComponent(content),
			type : type,
			page : 1,
			start : 0,
			limit : 20
        }
	});
	blackListGrid.currentPage=1
}

Ext.define("app.view.BlackListGrid",{
					extend : "Ext.grid.Panel",
					xtype : 'cell-editing',
					alias : "widget.blackListGrid",
					id : "blackListGrid",
					width : 700,
					height : 350,
					viewConfig : {
						loadMask : false
					// grid去掉加载中的提示
					},
					//			selModel : {
					//				selType : "checkboxmodel"
					//			},
					border : 0,
					multiSelect : true,
					frame : false, // 不要边框
					tbar : tbar,
					bbar : {
						xtype : 'pagingtoolbar',
						store : 'BlackListStore',
						dock : 'bottom',
						displayInfo : true
					},
					viewConfig : {
						enableTextSelection : true
					// 文本可选择
					},
					enableKeyNav : true,
					columnLines : true,
					forceFit : true, // 宽度自适配
					columns : [
							{
								text : "序号",
								dataIndex : "id",
								width : 20
							},{
								text : "类型",
								dataIndex : "type",
								width : 80,
								editor : {
					                xtype : 'combobox',
					                editable : false
					            },
								renderer : function(v, p, record, rowIndex, colIndex) { // 值自定义渲染
									if (v==1) {
							        	v = "<font style='color:red'>城市</font>";
							        }else if (v==2) {
							        	v = "<font style='color:blue'>用户</font>";
							        }
							        return v;
							    }
							},
							{
								text : "内容",
								dataIndex : "content",
								width : 120,
								editor : {
									xtype : "textfield",
									allowBlank : false
								}
							},
							{
								xtype : 'actioncolumn',
								width : 20,
								tdCls : 'action',
								text : '操作',
								sortable : false,
								menuDisabled : true,
								items : [
										{
											icon : 'js/ext-4.2/resources/ext-theme-neptune/images/icon/edit.png',
											tooltip : '编辑',
											scope : this,
											handler : function(grid, rowIndex,
													colIndex) {
												var record = grid.getStore()
														.getAt(rowIndex);
												var id = record.get('id');
												this.editBlackList(grid, id);
											},
											getClass : function(v, meta, record) {
												//		                    	var payStatue = record.get('statue');
												//		                        if (payStatue == '1') {
												//		                              return 'emptyCss';
												//		                        }else{
												//		                              return '';
												//		                        }
											}
										},
										{
											icon : 'js/ext-4.2/resources/ext-theme-neptune/images/icon/close.png',
											tooltip : '删除',
											scope : this,
											handler : function(grid, rowIndex,
													colIndex) {
												var record = grid.getStore()
														.getAt(rowIndex);
												var id = record.get('id');
												this.deleteBlackListById(grid,id, record);
											},
											getClass : function(v, meta, record) {
											}
										} ]
							} ],
					listeners : {
						beforeedit : function(editor, e, eOpts) {
							//		            if(e.field == 'createTime'){
							//		            	return false;
							//		            }else if(e.record.raw.username == 'admin' && (e.field == 'userType' || e.field == 'username')){
							//		            	return false;
							//		            }else{
							//		            	return true;
							//		            }
							return true;
						}
					},
					initComponent : function() {
						// 可编辑插件
						//				this.editing = Ext.create("Ext.grid.plugin.CellEditing");
						//				this.plugins = [this.editing];
						this.callParent(arguments);
					},
					store : "BlackListStore"
				});

var myMask = new Ext.LoadMask(Ext.getBody(), {
	msg : "正在加载中..."
});

//根据ID删除
function deleteBlackListById(grid, id, record) {
	var store = grid.getStore();
	Ext.Ajax.request({
		url : "deleteBlackListById",
		params : {
			id : id
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('(' + response.responseText + ')');
			if (result.resultCode == 'success') {
				store.load();
				Ext.example.msg('', '操作成功', '');
			} else {
				Ext.example.msg('', '操作失败<br/>' + result.errorInfo, '');
			}
		}
	});
}

//编辑支付方式
function editBlackList(grid, id) {

	var win = Ext.create('Ext.window.Window', {
		autoShow : false,
		title : '编辑黑名单',
		width : 370,
		height : 215,
		layout : 'fit',
		plain : true,
		modal : 'true', // 模态
		collapsible : false, //允许缩放条  
		closeAction : 'destroy',
		autoScroll : true,
		maximizable : true,
		loader : {
			url : './editBlackList?id=' + id,
			scripts : true,
			autoLoad : true
		},
		listeners : {
			'beforerender' : function() {
				myMask.show();
			}
		},
		buttonAlign : "center",
		buttons : [{
			text : '保存',
			handler : function() {
				if (validateBlack()) {
					var dataParm = decodeURIComponent($(
							"#black_list_form").serialize(), true);
					dataParm = encodeURI(encodeURI(dataParm));
					$.ajax({
						url : './saveBlackList?data=' + Math.random(),
						data : dataParm,
						type : "post",
						dataType : "json",
						success : function(data) {
							if (data.resultCode == 'error') {
								Ext.example.msg('',data.errorInfo, '');
								return;
							} else if (data.resultCode == 'success') {
								// 弹出新增成功消息框 
								Ext.example.msg('', '操作成功', '');
								// 关闭窗口
								win.close();
								search();
							}
						}
					});
				}
			}
		}, {
			text : '取消',
			handler : function() {
				// 关闭窗口
				win.close();
			}
		}]
	});

	win.show();
}

//添加黑名单
function addBlackList() {

	var win = Ext.create('Ext.window.Window', {
		autoShow : false,
		title : '新增黑名单',
		width : 370,
		height : 215,
		layout : 'fit',
		plain : true,
		modal : 'true', // 模态
		collapsible : false, //允许缩放条  
		closeAction : 'destroy',
		autoScroll : true,
		maximizable : true,
		loader : {
			url : 'addBlackList',
			scripts : true,
			autoLoad : true
		},
		listeners : {
			'beforerender' : function() {
				myMask.show();
			}
		},
		buttonAlign : "center",
		buttons : [
				{
					text : '保存',
					handler : function() {
						if (validateSpe()) {
							var dataParm = decodeURIComponent($(
									"#black_list_form").serialize(), true);
							dataParm = encodeURI(encodeURI(dataParm));
							$.ajax({
								url : './saveBlackList?data=' + Math.random(),
								data : dataParm,
								type : "post",
								dataType : "json",
								success : function(data) {
									if (data.resultCode == 'error') {
										Ext.example.msg('',data.errorInfo, '');
										return;
									} else if (data.resultCode == 'success') {
										// 弹出新增成功消息框 
										Ext.example.msg('', '操作成功', '');
										// 关闭窗口
										win.close();
										search();
									}
								}
							});
						}
					}
				}, {
					text : '取消',
					handler : function() {
						// 关闭窗口
						win.close();
					}
				} ]
	});

	win.show();
}