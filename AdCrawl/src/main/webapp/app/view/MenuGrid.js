function showErrorMsg(msg){
	Ext.example.msg('', msg, '');
}

// 获取主菜单
var getParentMenuStore = Ext.create('Ext.data.Store', {
     model: 'app.model.MenuModel',
     proxy: {
         type: 'ajax',
         url: 'getParentMenu',
         reader: {
             type: 'json',
             root: 'data'
         }
     },
     autoLoad: false
 });


var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout: {  
        overflowHandler: 'Menu'  
    },  
	items : [{
		id:'menuId',
		xtype : 'combo',
		fieldLabel : '菜单',
		labelWidth : 30,
		store : getParentMenuStore,
		displayField : 'menuName',
		valueField : 'id',
		name : 'menuId',
		typeAhead:false,
		editable:false,
		triggerAction : 'all',
		emptyText : '===请选择===',
		anchor : '95%',
		width : 170
	},{
		id:'key',
		xtype : 'textfield',
		name : 'key',
		fieldLabel : '关键字',
		labelWidth : 50,
		width : 360,
		emptyText : '==============请填写关键字============='
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("menuId").setValue("");
			Ext.getCmp("key").setValue("");
			search();
		}
	},{
		text : '新增子菜单',
		handler : function() {
			addChildMenu();
		}
	}]
});

function search(){
	var key = encodeURIComponent(Ext.getCmp("key").getValue());
	var menuId = Ext.getCmp("menuId").getValue();
	
	// 获取grid的store
	var siteStore = Ext.getCmp("menuGrid").getStore();
	siteStore.load({
		params : {
			key : key,
			parentId : menuId
        }
	});
}

Ext.define("app.view.MenuGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.menuGrid",
			id : "menuGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
			selModel : {
				//selType : "checkboxmodel"
			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : tbar,
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'MenuStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"序号",
						dataIndex:"id",
						width:26
					},{
						text : "父菜单名称",
						dataIndex : "parentMenuName",
						width : 70,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "子菜单名称",
						dataIndex : "menuName",
						width : 70,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "子菜单编号",
						dataIndex : "menuCode",
						width : 35,
						editor : {
							xtype : "textfield",
		                    allowBlank: false
			            }
					},{
		                xtype: 'actioncolumn',
		                width: 10,
		                tdCls: 'action',  
		                text: '操作',  
		                sortable: false,
		                menuDisabled: true,
		                items: [{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/edit.png',
		                    tooltip: '删除',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        this.deleteChildMenuById(grid,id);
		                    },
		                    getClass: function(v, meta, record) {
		                    	return '';
		                    }
		                }
		                ]
		            }
		            ],
			listeners: { 
			},					
			initComponent : function() {
				// 可编辑插件
				this.callParent(arguments);
			},
			store : "MenuStore"
});

// 根据Id删除子菜单
function deleteChildMenuById(grid,id){
	var store = grid.getStore();
	Ext.Ajax.request({
		url : "deleteChildMenuById",
		params : {
			id : id
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				// 刷新grid
				search();
				Ext.example.msg('', '删除成功', '');
			}else{
				Ext.example.msg('', '删除失败', '');
			}
		}
	});
}

// 新增子菜单
function addChildMenu(){
	var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '段子详情',
        width: 484,
        height: 303,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: 'true',  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'addChildMenuBefore',
            scripts: true,
            autoLoad: true
        },
        buttonAlign: "center", 
        buttons: [{
            text: '保存',
            handler: function() {
            	if(validate()){
		            		var dataParm = decodeURIComponent($("#add_info_form").serialize(),true);
			            	dataParm = encodeURI(encodeURI(dataParm));
	                    	$.ajax({
	          					type: "post",
	          					url: "./addChildMenu",
	          					data: dataParm,
	          					dataType : "json",
			        			success : function(data) {
			        				if (data.resultCode == 'error') {
			        					return;
			        				} else if (data.resultCode == 'success') {
			        					// 刷新grid
			        					search();
			        					// 弹出新增成功消息框 
			        					Ext.example.msg('', '操作成功', '');
			        					// 关闭窗口
			        					win.close();
			        				}
			        			}
	      					});
		            	}
            }
        },{
            text: '关闭',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}