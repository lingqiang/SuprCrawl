var getAppListAll=  Ext.create('Ext.data.Store',{
	   model:'app.model.ApplicationModel',
	   pageSize: 200000, 
	   proxy:{
		   type:'ajax',
		   url:'getAppFeeCensusKAll',
		   reader:{
			   type:'json',
			   root:'data',
		   },
		   writer:{
		       	type:'json'
		       }
	   },
	   autoLoad: true
	});


var getChannelAll=  Ext.create('Ext.data.Store',{
	   model:'app.model.ChannelModel',
	   pageSize: 200000, 
	   proxy:{
		   type:'ajax',
		   url:'getChannelAll',
		   reader:{
			   type:'json',
			   root:'data',
		   },
		   writer:{
		       	type:'json'
		       }
	   },
	   autoLoad: true
	});





var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout: {  
        overflowHandler: 'Menu'  
    },  
	items : [{
		id:'appName',
		xtype : 'combo',
		name : 'appName',
		fieldLabel : '产品名称',
		labelWidth : 60,
		editable:false,
		width : 190,
		emptyText : '全部产品',
		store : getAppListAll,
		displayField:'appName',
	    valueField:'id',
	    typeAhead:false,
	    triggerAction:'all',
        listeners:{
          	'change': function(val){
          		search(); 	
          	}
        }
	},{
		id:'channelname',
		xtype : 'combo',
		name : 'channelname',
		fieldLabel : '渠道名称',
		labelWidth : 60,
		editable:false,
		width : 190,
		emptyText : '全部渠道',
		store : getChannelAll,
		displayField:'channelName',
	    valueField:'id',
	    typeAhead:false,
	    triggerAction:'all',
        listeners:{
          	'change': function(val){
          		search(); 	
          	}
        }
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("payName").setValue("");
			Ext.getCmp("statue").setValue("");
			search();
		}
	}]
});

function search(){
	var payName = Ext.getCmp("payName").getValue();
	var statue = Ext.getCmp("statue").getValue();
	// 获取grid的store
	var payModeStore = Ext.getCmp("feeKGrid").getStore();
	payModeStore.load({
		params : {
			payName : encodeURIComponent(payName),
			statue:statue
        }
	});
}

Ext.define("app.view.FeeCensusKGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.feeKGrid",
			id : "feeKGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
//			selModel : {
//				selType : "checkboxmodel"
//			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : tbar,
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'FeeCensusKStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"日期",
						dataIndex:"statdate",
						width:70
					},{
						text : "产品名称",
						dataIndex : "appname",
						width : 70,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "渠道名称",
						dataIndex : "channelname",
						width : 70,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "新增用户",
						dataIndex : "realadduser",
						width : 70,
						sortable : false,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "活跃用户",
						dataIndex : "realactiveuser",
						width : 70,
						sortable : false,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "付费人数",
						dataIndex : "realchargeuser",
						width : 70,
						sortable : false,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "付费金额",
						dataIndex : "realchargemoney",
						width : 70,
						sortable : false,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					}],
			listeners: { 
				beforeedit: function (editor, e, eOpts) {
//		            if(e.field == 'createTime'){
//		            	return false;
//		            }else if(e.record.raw.username == 'admin' && (e.field == 'userType' || e.field == 'username')){
//		            	return false;
//		            }else{
//		            	return true;
//		            }
					return true;
		        }
			},					
			initComponent : function() {
				// 可编辑插件
//				this.editing = Ext.create("Ext.grid.plugin.CellEditing");
//				this.plugins = [this.editing];
				this.callParent(arguments);
			},
			store : "FeeCensusKStore"
});

var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"正在加载中..."});  

//根据id变更状态
function updatePayModeStatueById(grid,id,payStatue,record){
	var store = grid.getStore();
	if(payStatue == 0){
		payStatue = 1;
	}else{
		payStatue = 0;
	}
	
	Ext.Ajax.request({
		url : "updatePayModeStatueById",
		params : {
			id : id,
			statue : payStatue
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				// 刷新grid
//				store.load();
				// 只刷新这一行
				record.set("statue",payStatue);
				record.commit();
				Ext.example.msg('', '操作成功', '');
			}else{
				Ext.example.msg('', '操作失败', '');
			}
		}
	});
}

//根据ID删除
function deletePayModeById(grid,id,record){
	var store = grid.getStore();
	Ext.Ajax.request({
		url : "deletePayModeById",
		params : {
			id : id
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				store.load();
				Ext.example.msg('', '操作成功', '');
			}else{
				Ext.example.msg('', '操作失败<br/>'+result.errorInfo, '');
			}
		}
	});
}


//编辑支付方式
function editPayMode(grid,id){
	// 获取grid的store
	var payModeStore = grid.getStore();
	
    var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '编辑支付方式',
        width: 710,
        height: 480,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: 'true',  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'./editPayModel?id='+id,
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		myMask.show();  
        	}
        },
        buttonAlign: "center",  
        buttons: [{
            text: '保存',
            handler: function() {
            	if(validate()){
	            	var dataParm = decodeURIComponent($("#add_pay_mode_form").serialize(),true);
	            	dataParm = encodeURI(encodeURI(dataParm));
	            	$.ajax({
	        			url : './savePayMode?data='+Math.random(),
	        			data: dataParm, 
	        			type : "post",
	        			dataType : "json",
	        			success : function(data) {
	        				if (data.resultCode == 'error') {
	        					return;
	        				} else if (data.resultCode == 'success') {
	        					// 弹出新增成功消息框 
	        					Ext.example.msg('', '操作成功', '');
	        					// 关闭窗口
	        					win.close();
	        					search();
	        				}
	        			}
	        		});
            	}
            }
        },{
            text: '取消',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}

//添加支付方式
function addPayMode(){
	// 获取grid的store
	var payModeStore = Ext.getCmp("feeKGrid").getStore();
	
    var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '新增支付方式',
        width: 710,
        height: 480,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: 'true',  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'addPayModel',
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		myMask.show();  
        	}
        },
        buttonAlign: "center",  
        buttons: [{
            text: '保存',
            handler: function() {
            	if(validate()){
	            	var dataParm = decodeURIComponent($("#add_pay_mode_form").serialize(),true);
	            	dataParm = encodeURI(encodeURI(dataParm));
	            	$.ajax({
	        			url : './savePayMode?data='+Math.random(),
	        			data: dataParm, 
	        			type : "post",
	        			dataType : "json",
	        			success : function(data) {
	        				if (data.resultCode == 'error') {
	        					return;
	        				} else if (data.resultCode == 'success') {
	        					// 弹出新增成功消息框 
	        					Ext.example.msg('', '操作成功', '');
	        					// 关闭窗口
	        					win.close();
	        					search();
	        				}
	        			}
	        		});	
            	}
            }
        },{
            text: '取消',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}