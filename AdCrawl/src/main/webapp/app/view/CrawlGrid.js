function showErrorMsg(msg){
	Ext.example.msg('', msg, '');
}

//var tbar = Ext.create('Ext.toolbar.Toolbar', {
//	dock : 'top',
//	layout: {  
//        overflowHandler: 'Menu'  
//    },  
//	items : [{
//		id:'key',
//		xtype : 'textfield',
//		name : 'key',
//		fieldLabel : '关键字',
//		labelWidth : 50,
//		width : 360,
//		emptyText : '==============请填写关键字============='
//	},{
//		text : '查询',
//		handler : function() {
//			search();
//		}
//	},{
//		text : '重置',
//		handler : function() {
//			Ext.getCmp("key").setValue("");
//			search();
//		}
//	}]
//});

//function search(){
//	var key = encodeURIComponent(Ext.getCmp("key").getValue());
//	
//	var siteStore = Ext.getCmp("crawlGrid").getStore();
//	siteStore.load({
//		params : {
//			key : key,
//			page : 1,
//			start : 0,
//			limit : 20
//        }
//	});
//	siteStore.currentPage=1
//}

Ext.define("app.view.CrawlGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.crawlGrid",
			id : "crawlGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
			selModel : {
				//selType : "checkboxmodel"
			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : '',
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'CrawlStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"序号",
						dataIndex:"id",
						width:30
					},{
						text : "站点名称",
						dataIndex : "siteName",
						width : 60,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "分类名称",
						dataIndex : "categoryName",
						width : 60,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "状态",
						dataIndex : "statue",
						width : 60,
						editor : {
			                xtype : 'combobox',
			                editable : false,
			                store : [['0', '未开始'],
	                         ['1', '运行中'],
	                         ['2', '运行失败'],
	                         ['3', '运行完成']
			            ]},
						renderer : function(v, p, record, rowIndex, colIndex) { // 值自定义渲染
					        if(v == '0'){
					        	return "未开始";
					        }else if(v == '1'){
					        	return "<font style='color:red'>运行中</font>";
					        }else if(v == '2'){
					        	return "运行失败";
					        }else if(v == '3'){
					        	return "<font style='color:red'>运行完成</font>";
					        }
					    }
					},{
						text : "最后抓取成功时间",
						dataIndex : "lastCrawlTime",
						width : 70,
						editor : {
							xtype : "textfield",
		                    allowBlank: false
			            }
					},{
		                xtype: 'actioncolumn',
		                width: 20,
		                tdCls: 'action',  
		                text: '操作',  
		                sortable: false,
		                menuDisabled: true,
		                items: [{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/add.png',
		                    tooltip: '查看日志',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        var statue = record.get('statue');
		                        this.queryLogById(record,grid,id,statue);
		                    }
		                },{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/edit.png',
		                    tooltip: '编辑爬虫',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        var statue = record.get('statue');
		                        this.editCrawlById(record,grid,id,statue);
		                    }
		                }
		                ]
		            }
		            ],
			listeners: { 
			},					
			initComponent : function() {
				// 可编辑插件
				this.callParent(arguments);
			},
			store : "CrawlStore"
});

// 编辑爬虫
function editCrawlById(record,grid,id,statue){
	var siteName = record.get('siteName');
	var categoryName = record.get('categoryName');
	var store = grid.getStore();
	var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '['+siteName+'-'+categoryName+'] 爬虫设置',
        width: 900,
        height: 483,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: true,  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'editCrawlById?id='+id,
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		//myMask.show();  
        	}
       },
        buttonAlign: "center",  
        buttons: [{
            text: '保存',
            handler: function() {
            	var dataParm = decodeURIComponent($(
							"#update_info_form").serialize(), true);
					dataParm = encodeURI(encodeURI(dataParm));
					$.ajax({
						url : './updateCrawlInfo?data=' + Math.random(),
						data : dataParm,
						type : "post",
						dataType : "json",
						success : function(data) {
							if (data.resultCode == 'error') {
								Ext.example.msg('',data.errorInfo, '');
								return;
							} else if (data.resultCode == 'success') {
								// 弹出新增成功消息框 
								Ext.example.msg('', '操作成功', '');
								// 关闭窗口
								win.close();
							}
						}
					});
            }
        },{
            text: '关闭',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}

// 重启爬虫任务
function restartCrawlTask(grid,id){
	var store = grid.getStore();
	Ext.Ajax.request({
		url : "restartCrawlTaskById",
		params : {
			id : id
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				store.load();
				Ext.example.msg('', '操作成功', '');
			}else{
				Ext.example.msg('', '操作失败', '');
			}
		}
	});
}


// 查询爬虫任务日志
function queryLogById(record,grid,id,statue){
	var siteName = record.get('siteName');
	var categoryName = record.get('categoryName');
	var store = grid.getStore();
	var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '['+siteName+'-'+categoryName+'] 爬虫日志',
        width: 1111,
        height: 483,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: true,  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'getCrawlLogById?id='+id,
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		//myMask.show();  
        	}
       },
        buttonAlign: "center",  
        buttons: [{
            text: '重启爬虫',
            handler: function() {
            	Ext.Ajax.request({
					url : "restartCrawlTaskById",
					params : {
						id : id
					},
					type : "POST",
					timeout : 4000,
					success : function(response, opts) {
						var result = eval('('+response.responseText+')');
						if(result.resultCode == 'success'){
							Ext.example.msg('', '操作成功', '');
						}else{
							Ext.example.msg('', '操作失败', '');
						}
					}
				});
            }
        },{
            text: '停止爬虫',
            handler: function() {
            	Ext.Ajax.request({
					url : "stopCrawlTaskById",
					params : {
						id : id
					},
					type : "POST",
					timeout : 4000,
					success : function(response, opts) {
						var result = eval('('+response.responseText+')');
						if(result.resultCode == 'success'){
							Ext.example.msg('', '操作成功', '');
						}else{
							Ext.example.msg('', '操作失败', '');
						}
					}
				});
            }
        },{
            text: '清除日志',
            handler: function() {
				$("#crawlInfo").html("");            	
            }
        },{
            text: '关闭',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}