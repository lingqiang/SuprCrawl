var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout: {  
        overflowHandler: 'Menu'  
    },  
	items : [{
		id:'appNameParam',
		xtype : 'textfield',
		name : 'appName',
		fieldLabel : '应用名称',
		labelWidth : 60,
		width : 220,
		emptyText : '===请填写应用名称===',
		listeners:{
			'blur':function(){
				search();
			}
		}
	},{
		id:'payStatue',
		xtype : 'combo',
		name : 'payStatue',
		fieldLabel : '支付状态',
		labelWidth : 60,
		editable:false,
		width : 190,
		emptyText : '===请选择===',
		store : [['0', '未设置'],
                  ['1', '关闭'],
                  ['2', '开启']],
        listeners:{
          	'change': function(val){
          		search(); 	
          	}
        }
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("appNameParam").setValue("");
			Ext.getCmp("payStatue").setValue("");
			search();
		}
	}]
});

function search(){
	var appName = Ext.getCmp("appNameParam").getValue();
	var payStatue = Ext.getCmp("payStatue").getValue();
	// 获取grid的store
	var payConfigStore = Ext.getCmp("payConfigGrid").getStore();
	payConfigStore.load({
		params : {
			appName : encodeURIComponent(appName),
			payStatue:payStatue
        }
	});
}

Ext.define("app.view.PayConfigGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.payConfigGrid",
			id : "payConfigGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
//			selModel : {
//				selType : "checkboxmodel"
//			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : tbar,
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'PayConfigStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"序号",
						dataIndex:"id",
						width:20
					},{
						text : "应用名称",
						dataIndex : "appName",
						width : 200,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "状态",
						dataIndex : "payStatue",
						width : 130,
						editor : {
			                xtype : 'combobox',
			                editable : false
			            //    disabled:true,
			            },
						renderer : function(v, p, record, rowIndex, colIndex) { // 值自定义渲染
					        if (v==0) {
					        	v = "<font style='text-decoration:line-through;'>未设置</font>";
					        }else if (v==1) {
					        	v = "<font style='color:gray'>关闭</font>";
					        }else if (v==2) {
					        	v = "<font style='color:red'>开启</font>";
					        }
					        return v;
					    }
					},{
		                xtype: 'actioncolumn',
		                width: 25,
		                tdCls: 'action',  
		                text: '操作',  
		                sortable: false,
		                menuDisabled: true,
		                items: [{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/add.png',
		                    tooltip: '基本设置',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        this.baseConfig(grid,id,record);
		                    },
		                    getClass: function(v, meta, record) {
		                    	/*var userType = record.get('userType');
		                        if (userType == '1') {
		                              return 'emptyCss';
		                        }else{
		                              return '';
		                        }*/
		                    }
		                },{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/special.png',
		                    tooltip: '特殊设置',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        this.specialConfigGrid(grid,id);
		                    }
		                },{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/change.png',
		                    tooltip: '状态切换',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                        var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        var payStatue = record.get('payStatue');
		                        this.updatePayStatueById(grid,id,payStatue,record);
		                    },
		                    getClass: function(v, meta, record) {
		                    	var payStatue = record.get('payStatue');
		                        if (payStatue == '0') {
		                              return 'emptyCss';
		                        }else{
		                              return '';
		                        }
		                    }
		                }]
		            }],
			listeners: { 
				beforeedit: function (editor, e, eOpts) {
//		            if(e.field == 'createTime'){
//		            	return false;
//		            }else if(e.record.raw.username == 'admin' && (e.field == 'userType' || e.field == 'username')){
//		            	return false;
//		            }else{
//		            	return true;
//		            }
					return true;
		        }
			},					
			initComponent : function() {
				// 可编辑插件
//				this.editing = Ext.create("Ext.grid.plugin.CellEditing");
//				this.plugins = [this.editing];
				this.callParent(arguments);
			},
			store : "PayConfigStore"
});

//根据id变更状态
function updatePayStatueById(grid,id,payStatue,record){
	var store = grid.getStore();
	if(payStatue == 1){
		payStatue = 2;
	}else{
		payStatue = 1;
	}
	
	Ext.Ajax.request({
		url : "updatePayStatueById",
		params : {
			id : id,
			payStatue : payStatue
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				// 刷新grid
//				store.load();
				// 只刷新这一行
				record.set("payStatue",payStatue);
				record.commit();
				Ext.example.msg('', '操作成功', '');
			}else{
				Ext.example.msg('', '操作失败', '');
			}
		}
	});
}

var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"正在加载中..."});  


//应用支付基本设置
function baseConfig(grid,id,record){
	var store = grid.getStore();
	
    var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '基本设置',
        width: 980,
        height: 480,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: false,  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy', 
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'basePayConfig?data='+Math.random()+'&gameId='+id,
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		myMask.show();  
        	}
//        	,
//        	'afterrender':function(){
//        		myMask.hide();  
//        	}
        },
        buttonAlign: "center",  
        buttons: [{
            text: '保存',
            handler: function() {
            	// 校验
            	if(validate() && auto()){  // 自动补全hidden内容
                	var dataParm = decodeURIComponent($("#app_info_form").serialize(),true);
                	dataParm = encodeURI(encodeURI(dataParm));
                	$.ajax({
            			url : './saveAppBasePayConfig?data='+Math.random(),
            			data: dataParm,
            			type : "post",
            			dataType : "json",
            			async: true,
            			success : function(data) {
            				if (data.resultCode == 'error') {
            					return;
            				} else if (data.resultCode == 'success') {
            					// 弹出新增成功消息框 
            					Ext.example.msg('', '操作成功', '');
            					// 修改当前记录的状态  为开启
            					if(record.get("payStatue")==0){
            						record.set("payStatue",2);
            					}
            					record.commit();
            					// 关闭窗口
            					win.close();
            				}
            			}
            		});
            	}
            }
        },{
            text: '取消',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}

//应用支付特殊设置
function specialConfigGrid(grid,id){
	var store = grid.getStore();
	var tbar = Ext.create('Ext.toolbar.Toolbar', {
		dock : 'top',
		layout: {  
	        overflowHandler: 'Menu'  
	    },  
		items : [{
			text : '添加',
			align: "center",  
			handler : function() {
				specialConfig(grid,id);
			}
		}]
	});
	
	var specialPayConfigModel = Ext.define('specialPayConfigModel', {
		extend: 'Ext.data.Model',
		fields: [
	     {name:'id',type:'int',sortable:true},
	     {name:'versionName',type:'String',sortable:true},
	     {name:'channelName',type:'String',sortable:true},
	     {name:'configStr',type:'String',sortable:true},
	     {name:'applicationId',type:'int',sortable:true}
	    ]
	});
	
	var specialPayCofigStore = Ext.create('Ext.data.Store', {
		model: specialPayConfigModel,
		pageSize: 20, // 每页显示条数
        proxy: {
             type: 'ajax',
             api: {
             	read: './getSpecialPayConfigList?gameId='+id,  // 查询
             },
             reader: {
                 type: 'json',
                 root: 'data',
                 totalProperty: 'totalCount'
             },
             writer:{
             	type:'json'
             }
         },
        autoLoad: true
	});
	
	var grid = Ext.create("Ext.grid.Panel", {
        renderTo: Ext.getBody(),
        border : 0,
        frame: false,
        tbar : tbar,
        viewConfig: {
            forceFit: true,
            stripeRows: true
        },
        bbar : {
    		xtype : 'pagingtoolbar',
    		store:specialPayCofigStore,
    		dock : 'bottom',
    		displayInfo : true
    	},
    	enableKeyNav : true,
    	columnLines : true,
    	forceFit : true, // 宽度自适配
        columns: [{
			text:"序号",
			dataIndex:"id",
			width:80
		},{
			text:"版本",
			dataIndex:"versionName",
			width:100
		},{
			text:"渠道",
			dataIndex:"channelName",
			width:100
		},{
			text:"支付方式",
			dataIndex:"configStr",
			width:300
		},{
			text:"应用Id",
			dataIndex:"applicationId",
			hidden:true,
			width:300
		},{
            xtype: 'actioncolumn',
            width: 40,
            tdCls: 'action',  
            text: '操作',  
            sortable: false,
            menuDisabled: true,
            items: [{
                icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/close.png',
                tooltip: '删除',
                scope: this,
                handler: function(grid, rowIndex, colIndex) {
                	var record = grid.getStore().getAt(rowIndex);
                    var id = record.get('id');
                    this.delSpecialConfig(grid,id,record);
                },
                getClass: function(v, meta, record) {
                }
            },{
                icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/edit.png',
                tooltip: '编辑',
                scope: this,
                handler: function(grid, rowIndex, colIndex) {
                	var record = grid.getStore().getAt(rowIndex);
                    var id = record.get('id');
                    var gameId = record.get('applicationId');
                    this.editSpecialConfig(grid,id,gameId,record);
                },
                getClass: function(v, meta, record) {
                }
            }]
        }],
		store:specialPayCofigStore
    });
	
    var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '特殊设置列表',
        width: 980,
        height: 500,
        layout: 'fit',
        plain:true,
        modal: false,  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        items: [grid],
        buttonAlign: "center",  
        buttons: [{
            text: '关闭',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}

//应用支付特殊设置
function specialConfig(grid,id){
	var store = grid.getStore();
    var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '特殊设置',
        width: 980,
        height: 500,
        layout: 'fit',
        plain:true,
        modal: false,  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy', 
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'addSpecialPayConfig?data='+Math.random()+'&gameId='+id,
            scripts: true,
            autoLoad: true
        },
        listeners:{
//        	'beforerender':function(){
//        		myMask.show();  
//        	}
        },
        buttonAlign: "center",  
        buttons: [{
            text: '保存',
            handler: function() {
            	// 校验
            	if(validateSpe() && auto2()){ // 自动补全hidden内容
                	var dataParm = decodeURIComponent($("#app_info_form").serialize(),true);
                	dataParm = encodeURI(encodeURI(dataParm));
                	$.ajax({
            			url : './saveAppSpecialPayConfig?data='+Math.random(),
            			data: dataParm,
            			type : "post",
            			dataType : "json",
            			async: true,
            			success : function(data) {
            				if (data.resultCode == 'error') {
            					return;
            				} else if (data.resultCode == 'success') {
            					// 弹出新增成功消息框 
            					Ext.example.msg('', '操作成功', '');
            					store.load();
            					// 关闭窗口
            					win.close();
            				}
            			}
            		});
            	}
            }
        },{
            text: '取消',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}

//编辑应用支付特殊设置
function editSpecialConfig(grid,id,gameId,record){
	var store = grid.getStore();
    var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '编辑特殊设置',
        width: 980,
        height: 500,
        layout: 'fit',
        plain:true,
        modal: false,  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy', 
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'editSpecialPayConfig?data='+Math.random()+'&Id='+id+"&gameId="+gameId,
            scripts: true,
            autoLoad: true
        },
        listeners:{
//        	'beforerender':function(){
//        		myMask.show();  
//        	}
        },
        buttonAlign: "center",  
        buttons: [{
            text: '保存',
            handler: function() {
            	// 校验
            	if(validateSpe() && auto2()){ // 自动补全hidden内容
                	var dataParm = decodeURIComponent($("#app_info_form").serialize(),true);
                	dataParm = encodeURI(encodeURI(dataParm));
                	$.ajax({
            			url : './saveAppSpecialPayConfig?data='+Math.random(),
            			data: dataParm,
            			type : "post",
            			dataType : "json",
            			async: true,
            			success : function(data) {
            				if (data.resultCode == 'error') {
            					return;
            				} else if (data.resultCode == 'success') {
            					// 弹出新增成功消息框 
            					Ext.example.msg('', '操作成功', '');
            					store.load();
            					// 关闭窗口
            					win.close();
            				}
            			}
            		});
            	}
            }
        },{
            text: '取消',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}

//应用支付特殊设置
function delSpecialConfig(grid,id,record){
	var store = grid.getStore();
	Ext.Ajax.request({
		url : "delSpecialConfig",
		params : {
			id : id
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				store.load();
				Ext.example.msg('', '操作成功', '');
			}else{
				Ext.example.msg('', '操作失败<br/>'+result.errorInfo, '');
			}
		}
	});
}
	