Ext.define("app.controller.ArticleStoreController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["ArticleStoreGrid"],
	stores : ["ArticleStoreStore"],
	models : ["ArticleStoreModel","SiteModel","SiteCategoryModel"]
});