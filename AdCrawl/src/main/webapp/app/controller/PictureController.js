Ext.define("app.controller.PictureController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["PictureGrid"],
	stores : ["PictureStore"],
	models : ["PictureModel","MenuModel"]
});