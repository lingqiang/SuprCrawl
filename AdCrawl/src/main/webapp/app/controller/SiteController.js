Ext.define("app.controller.SiteController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["SiteGrid"],
	stores : ["SiteStore"],
	models : ["SiteModel"]
});