Ext.define("app.controller.MenuController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["MenuGrid"],
	stores : ["MenuStore"],
	models : ["MenuModel"]
});