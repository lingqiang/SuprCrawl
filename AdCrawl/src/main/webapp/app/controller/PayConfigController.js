Ext.define("app.controller.PayConfigController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
		this.control({
			
		});
	},
	views  : ["PayConfigGrid"],
	stores : ["PayConfigStore"],
	models : ["PayConfigModel"]
});