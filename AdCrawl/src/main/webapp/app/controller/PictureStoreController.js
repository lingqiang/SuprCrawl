Ext.define("app.controller.PictureStoreController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["PictureStoreGrid"],
	stores : ["PictureStoreStore"],
	models : ["PictureStoreModel","SiteModel"]
});