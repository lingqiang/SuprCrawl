Ext.define("app.controller.CrawlController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["CrawlGrid"],
	stores : ["CrawlStore"],
	models : ["CrawlModel"]
});

//打开浏览器
function openBrower(){
	// 输入url地址
	var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: 'Xpath浏览器',
        width: 610,
        height: 483,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: 'true',  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'addChildMenuBefore',
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		//myMask.show();  
        	}
       },
        buttonAlign: "center",  
        buttons: [{
            text: '上一个',
            handler: function() {
            }
        },{
            text: '删除',
            handler: function() {
            }
        },{
            text: '下一个',
            handler: function() {
            }
        }]
    });
    
    win.show();
	
}