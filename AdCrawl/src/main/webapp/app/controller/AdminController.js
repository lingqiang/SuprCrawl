//修改信息仅能修改一条信息标识
var flag = -1;

Ext.define("app.controller.AdminController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
		this.control({
			"admingrid button[id=add]" : {
				click : function(button) {
					var grid = this.getGrid(button);
					var store = grid.getStore();
					
					var edit = grid.editing;
					var model = store.model;
					// 新增模板
					var modelObj = {
						userType:'2',
						state:'0'
					};
					var adminObj = new model(modelObj);
					edit.cancelEdit();
					var newRecords = store.getNewRecords(); 
					
					if(newRecords.length<1)
						store.insert(0, adminObj);
					
					edit.startEditByPosition({
						row : 0,
						column : 2
					});
				}
			},
			"admingrid button[id=save]" : {
				click : function(button) {
					var grid = this.getGrid(button);
					var store = grid.getStore();
					var updateRecords = store.getUpdatedRecords(); // 更新的数据
					var newRecords = store.getNewRecords(); // 新增的数据
					
					if(newRecords.length == 1){
						// 新增单个
						var newData = newRecords[0].data;
						
						if(!validate(newData)){
							return;
						}
						
						Ext.Ajax.request({
							url : "addAdminInfo",
							jsonData : Ext.encode(newRecords[0].data),
							type : "post",
							timeout : 4000,
							success : function(response, opts) {
								var result = eval('('+response.responseText+')');
								if(result.resultCode == 'success'){
									// 刷新grid
									store.load();
									Ext.example.msg('', '新增成功', '');
								}else if(result.resultCode == 'fail'){
									Ext.example.msg('', '用户名不能重复', '');
								}else{
									Ext.example.msg('', '新增失败:'+result.errorInfo, '');
								}
							}
						});
					}else if(newRecords.length > 1){
						/*store.sync({	// 同步数据到后台
							success:function(){
								Ext.Array.each(newRecords, function(model) {
									model.commit(); // 前端提交  只是简单的修改前端红点样式
								});
								// 刷新grid
								store.load();
								Ext.example.msg('', '新增成功', '');
							},
							failure:function(){
								Ext.example.msg('', '新增失败', '');
							}
						}); */
					}
					
					if(updateRecords.length == 1){
						
						var newData = updateRecords[0].data;
						if(!validate(newData)){
							return;
						}
						
						// 更新单个
						Ext.Ajax.request({
							url : "updateAdminInfo",
							jsonData : Ext.encode(updateRecords[0].data),
							type : "post",
							timeout : 4000,
							success : function(response, opts) {
								var result = eval('('+response.responseText+')');
								if(result.resultCode == 'success'){
									// 刷新grid
									store.load();
									//将修改表示初始化
									flag=-1;
									Ext.example.msg('', '更新成功', '');
								}else if(result.resultCode == 'fail'){
									Ext.example.msg('', '用户名不能重复', '');
								}else{
									Ext.example.msg('', '更新失败', '');
								}
							}
						});
					}else if(updateRecords.length > 1){
						
						var sing = true;
						Ext.Array.each(updateRecords, function(model) {
							// 校验
							if(sing){
								if(!validate(model.data)){
									sing = false;
									return;
								}
							}
						});
						
						if(sing){
							store.sync({	// 同步数据到后台
								success:function(){
									Ext.Array.each(updateRecords, function(model) {
										model.commit(); // 前端提交  只是简单的修改前端红点样式
									});
									// 刷新grid
									store.load();
									Ext.example.msg('', '更新成功', '');
								},
								failure:function(){
									Ext.example.msg('', '更新失败', '');
								}
							});
						}
					}
					
				}
			},
			"admingrid":{
				edit:function(editor, e){ //监听edit事件
//					e.record.commit(); // 前端提交 没太大意义
					// 校验输入
					
				}
			}
			
		});
	},
	views  : ["AdminGrid"],
	stores : ["AdminStore"],
	models : ["AdminModel"]
});

function validate(newData){
	var sing = true;
	var loginName = newData.userName;
	var pwd = newData.userPwd;
	var nickName = newData.nickName;
	
	var message = "";
	
	if(loginName==""){
		message+= '用户名不能为空,';
		sing = false;
	}else if(!(/^\w+$/.test(loginName))){
		Ext.example.msg('', '用户名只能为字符数据组合', '');
		return false;
	}
	if(pwd==""){
		message+= '密码不能为空,';
		sing = false;
	}else if(pwd.length > 20){
		Ext.example.msg('', '密码长度不能超过20个字符', '');
		return false;
	}else if(!(/^(\w|[-_\~!@#\$%\^&\*|.])+$/.test(pwd))){
		Ext.example.msg('', '密码不合法', '');
		return false;
	} 
	if(nickName==""){
		message+= '姓名不能为空';
		sing = false;
	}else if(!(/^[\u0391-\uFFE5]+$/.test(nickName))){
		Ext.example.msg('', '姓名只能是中文', '');
		return false;
	}else if(nickName.length > 18){
		Ext.example.msg('', '姓名长度不能超过18个字符', '');
		return false;
	}
	if(!sing){
		Ext.example.msg('', message, '');
	}
	return sing;
}
