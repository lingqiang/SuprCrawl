Ext.define("app.controller.ArticleController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["ArticleGrid"],
	stores : ["ArticleStore"],
	models : ["ArticleModel","MenuModel"]
});