Ext.define("app.controller.PayModeController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
		this.control({
			
		});
	},
	views  : ["PayModeGrid"],
	stores : ["PayModeStore"],
	models : ["PayModeModel"]
});