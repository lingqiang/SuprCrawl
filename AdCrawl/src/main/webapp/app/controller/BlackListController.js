Ext.define("app.controller.BlackListController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
		this.control({
			
		});
	},
	views  : ["BlackListGrid"],
	stores : ["BlackListStore"],
	models : ["BlackListModel"]
});