Ext.define("app.store.CrawlStore",{
	extend:'Ext.data.Store',
	model: 'app.model.CrawlModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getCrawlList',  // 查询
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    autoLoad: true
});