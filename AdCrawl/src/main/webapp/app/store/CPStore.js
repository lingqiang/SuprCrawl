Ext.define("app.store.CPStore",{
	extend:'Ext.data.Store',
	model: 'app.model.CPModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getCPList',  // 查询
        	update: 'updateCPList',  // 更新
        	create: 'addCPList', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    autoLoad: true
});