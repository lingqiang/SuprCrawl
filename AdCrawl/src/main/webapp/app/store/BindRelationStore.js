Ext.define("app.store.BindRelationStore",{
	extend:'Ext.data.Store',
	model: 'app.model.BindRelationModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getBindRelationList',  // 查询
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    listeners: {//用于在分页的时候追加查询参数   
    	"beforeload" : function(store) { 
    		var menuId = Ext.getCmp("menuId").getValue()==-1?'':Ext.getCmp("menuId").getValue();
    		var siteId = Ext.getCmp("siteId").getValue()==-1?'':Ext.getCmp("siteId").getValue();
    		var siteCategoryId = Ext.getCmp("siteCategoryId").getValue()==-1?'':Ext.getCmp("siteCategoryId").getValue();
    		
    		Ext.apply(store.proxy.extraParams, {
    			menuId:menuId,
    			siteId: siteId,
    			siteCategoryId:siteCategoryId
    		});
    	}      
    },
    autoLoad: true
});