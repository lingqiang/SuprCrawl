Ext.define("app.store.ArticleStoreStore",{
	extend:'Ext.data.Store',
	model: 'app.model.ArticleStoreModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getArticleStoreList',  // 查询
        //	update: 'updateAppList',  // 更新
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    listeners: {//用于在分页的时候追加查询参数   
    	"beforeload" : function(store) { 
    		var content = encodeURIComponent(Ext.getCmp("content").getValue());
    		var siteId = Ext.getCmp("siteId").getValue();
    		var siteCategoryId = Ext.getCmp("siteCategoryId").getValue();
    		var type = Ext.getCmp("type").getValue();
    		var statue = Ext.getCmp("statue").getValue();
    		
    		Ext.apply(store.proxy.extraParams, {
    			content : content,
    			siteId:siteId,
    			siteCategoryId : siteCategoryId,
    			type:type,
    			statue:statue
    		});
    	}      
    },
    autoLoad: true
});