Ext.define("app.store.ArticleStore",{
	extend:'Ext.data.Store',
	model: 'app.model.ArticleModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getArticleList',  // 查询
        //	update: 'updateAppList',  // 更新
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    listeners: {//用于在分页的时候追加查询参数   
    	"beforeload" : function(store) { 
    		var content = encodeURIComponent(Ext.getCmp("content").getValue());
    		var menuId = Ext.getCmp("menuId").getValue();
    		var type = Ext.getCmp("type").getValue();
    		var createTime = Ext.getCmp("createTime").getValue()==null ? '' : dateTimeFormat(Ext.getCmp("createTime").getValue(), 'yyyy-MM-dd');
    		
    		Ext.apply(store.proxy.extraParams, {
    			content : content,
    			menuId:menuId,
    			type:type,
    			createTime:createTime
    		});
    	}      
    },
    autoLoad: true
});