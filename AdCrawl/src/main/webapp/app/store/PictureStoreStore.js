Ext.define("app.store.PictureStoreStore",{
	extend:'Ext.data.Store',
	model: 'app.model.PictureStoreModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getPictureStoreList',  // 查询
        //	update: 'updateAppList',  // 更新
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    autoLoad: true
});