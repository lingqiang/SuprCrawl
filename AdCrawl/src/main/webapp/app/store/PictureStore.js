Ext.define("app.store.PictureStore",{
	extend:'Ext.data.Store',
	model: 'app.model.PictureModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getPictureList',  // 查询
        //	update: 'updateAppList',  // 更新
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    autoLoad: true
});