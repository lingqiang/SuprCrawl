var leftMenu;

// 程序入口
Ext.onReady(function(){
	
	leftMenu = [
//	            {id:"1", text: "段子管理", leaf: false, expanded: true,children: [
//	          	    	                        	    	                {functionId:"11",url:"articleList", text: "段子列表", leaf: true },
//	          	    	                        	    	              {functionId:"12",url:"articleStoreList", text: "段子仓库列表", leaf: true }
//	          	    	                        	    	            ] },	    	     
//	          	{id:"2",  text: "图片管理", leaf: false , expanded: true,children: [
//	          	    	                         	    	              {functionId:"21",url:"picList", text: "相册列表", leaf: true },
//	          	    	                        	    	              {functionId:"22",url:"picStoreList", text: "相册仓库列表", leaf: true }
//	          	    	                        	    	            ] },
//	          	{id:"3",  text: "分类管理", leaf: false , expanded: true,children: [
//	          	    	                        	    		          {functionId:"31",url:"siteList", text: "爬取站点", leaf: true },
//	          	    	                        	    		          {functionId:"32",url:"siteCategoryList", text: "爬取分类列表", leaf: true },
//	          	    	                        	    		          {functionId:"33",url:"menuList", text: "菜单列表", leaf: true }
//	          	    	                        	    		        ] },
//	            {id:"4",  text: "爬虫管理", leaf: false , expanded: true,children: [
//	          	    	                        	  		          	{functionId:"41",url:"crawlList", text: "爬虫列表", leaf: true },
//	          	    	                        	  		          	{functionId:"42",url:"addArticleCrawl", text: "新增段子爬虫", leaf: true },
//	          	    	                        	  		          	{functionId:"44",url:"addArticleCrawlNew", text: "新增段子爬虫(新)", leaf: true },
//	          	    	                        	  		          	{functionId:"43",url:"addAlbumCrawl", text: "新增图片爬虫", leaf: true }
//	          	    	                        	  		          	] },
//				{id:"5", text: "绑定关系", leaf: false, expanded: true,children: [
//					                        	    	                {functionId:"51",url:"bindRelationList", text: "绑定关系列表", leaf: true }
//					                        	    	            ] },
				{id:"6", text: "名画管理", leaf: false, expanded: true,children: [
					                        	    					{functionId:"61",url:"addPaint", text: "名画录入", leaf: true }
					                        	    				] }
	          ];
	
	// 初始化tip提示
	Ext.QuickTips.init();  

	Ext.Loader.setConfig({
		enabled: true,
	});
	
	Ext.application({
		name: 'app', // 应用名称
		appFolder:'app', // 应用目录
	    launch: function() { // 当前页面加载完成执行的函数
	    	
	    	// 上
	    	var topPanel = Ext.create('Ext.panel.Panel', {  
	    		region: 'north',
    	        border: false,
    	        margins: '0 0 5 0',
    	        loader : {
    	            url : 'goHeader',
    	            autoLoad:true,
					scripts:true
    	        }
            });  
	    	
	    	// 左
	    	var leftPanel = Ext.create('Ext.panel.Panel', {  
	    		region : 'west',  
                title : '导航栏',  
                width : 180,  
                layout : 'fit',  
                collapsible : true,
                split:true
            });  
	    	
	    	// 下
	    	var bottomPanel = Ext.create('Ext.panel.Panel', {  
	    		region : 'south',  
	    		height: 15,
	    		html:"<div class='user-opper'>欢迎您，<span class='user-info'>管理员</span><span class='user-role'>【管理员】</span><a href='./loginout' class='exit-a'>退出</a></div>"
            });
	    	
	    	// 中间
	    	var rightPanel = Ext.create('Ext.tab.Panel', {  
	    		id : 'tab',
                region : 'center',  
                layout : 'fit',  
                tabWidth : 120,
                enableTabScroll: true ,
                resizeTabs: true,
                activeTab: 0, // 默认激活第一个tab页
                defaults: {
                    autoScroll: true
                },
                items: [{
                    title: "首页",
                    html: "<div class='index-img'>" +
                    		"<h1 class='home-h1'>欢迎使用</h1>"+
                    		"<h3 class='home-h3'>趣乐运营管理后台</h3>"+
                    		"<img src='./images/index-img.jpg'/></div>"
                }],
                plugins: Ext.create('Ext.ux.TabCloseMenu', {
                    extraItemsTail: [
                        '-',
                        {
                            text: 'Closable',
                            checked: true,
                            hideOnClick: true,
                            handler: function (item) {
                                currentItem.tab.setClosable(item.checked);
                            }
                        },
                        '-',
                        {
                            text: 'Enabled',
                            checked: true,
                            hideOnClick: true,
                            handler: function(item) {
                                currentItem.tab.setDisabled(!item.checked);
                            }
                        }
                    ],
                    listeners: {
                        beforemenu: function (menu, item) {
                            var enabled = menu.child('[text="Enabled"]'); 
                            menu.child('[text="Closable"]').setChecked(item.closable);
                            if (item.tab.active) {
                                enabled.disable();
                            } else {
                                enabled.enable();
                                enabled.setChecked(!item.tab.isDisabled());
                            }

                            currentItem = item;
                        }
                    }
                })
            });
	    	
	    	// 构造树
	    	var menuStore = Ext.create('Ext.data.TreeStore', {
	    	    root: {
	    	        expanded: true,
	    	        children: leftMenu
	    	    }
	    	});
	    	
	    	var menuTree = Ext.create('Ext.tree.Panel', {
	    	    animate:true,  
	    	    height: 150,
	    	    store: menuStore,
	    	    rootVisible: false,
	    	    listeners: {
                    itemclick: function(thisView, record, htmlElementItem, indexNo){
                    	if(record.raw.leaf){
                    		//alert("点击的节点ID是："+thisView+",url是："+record.raw.url+"__"+indexNo);
                    		var tabs = Ext.getCmp(record.raw.functionId); 
                    		if(tabs){
                    			rightPanel.setActiveTab(tabs);
                    		}else{
                    			var content = '<iframe width=100% height=100% frameborder=0 scrolling=yes marginheight=0 marginwidth=0 src="' + record.raw.url + '"></iframe>';
                    			var tab = rightPanel.add({
        		            		id:record.raw.functionId,
                            		title: record.raw.text,
                            		html:content,
                            		closable: true  // 允许关闭
                            	});
                            	
                            	rightPanel.setActiveTab(tab); // 设置当前tab页        
                    		}
                    	}
                    }
                } 
	    	});
	    	
	    	// 添加菜单树到左边菜单
	    	leftPanel.add(menuTree);
	    	
	    	Ext.create('Ext.container.Viewport', {
	    	    layout: 'border', // 布局方式
	    	    renderTo : Ext.getBody(), 
	    	    items: [leftPanel,rightPanel,bottomPanel]
	    	});
	    }
	})
	
})