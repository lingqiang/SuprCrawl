// 定义变量集合  用于保存全局变量
var map = Ext.create("Ext.util.MixedCollection");
//map.add('key1', 'val1');

//js获取项目根路径，如： http://localhost:8080/GameFngine
function getRootPath(){
    //获取当前网址，如： http://localhost:8080/GameFngine/share/meun.jsp
    var curWwwPath=window.document.location.href;
    //获取主机地址之后的目录，如： GameFngine/share/meun.jsp
   var pathName=window.document.location.pathname;
   var pos=curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8080
   var localhostPaht=curWwwPath.substring(0,pos);
    //获取带"/"的项目名，如：/GameFngine
    var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return(localhostPaht+projectName+"/upload/");
}