<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
	body,div,input,label,h1,ul,li{margin: 0; padding: 0;}
	body, button, input, select, textarea {font: 12px "微软雅黑",arial,sans-serif;color:#2e2e2e;}
	table {border-collapse:collapse;border-spacing:0;}
	fieldset,img {border:0}
	address,caption,cite,code,dfn,strong,th,var{font-style:normal;font-weight:normal}
	ol,ul {list-style:none}
	caption,th {text-align:left}
	h1,h2,h3,h4,h5,h6 {font-size:100%;font-weight:normal}
	q:before,q:after {content:''}
	abbr,acronym { border:0}
	a{text-decoration:none;color:#999;outline:none;hide-focus:expression(this.hideFocus=true);}
	em{font-style:normal;}
	input,a,textarea{ outline:none;}
	a:hover{ text-decoration:underline;}
	html,body{width:100%;height:100%;}
	.clearfix:after{content:".";display:block;height:0;clear:both;visibility:hidden}
	.clearfix{display:inline-block}* html .clearfix{height:1%}.clearfix{display:block}
	.fl{float:left;_display:inline}
	.p20{ padding: 20px;}.pt15{padding-top: 15px;}.pl20{padding-left: 20px;}.pb10{padding-bottom: 10px;}
	.m20{margin-left: 10px;}.ml10{margin-left:10px;}.mt20{margin-top:20px;}
	.pr{position: relative;}
	.h1-title{ font-size: 24px; line-height: 50px; font-weight: normal;}
	.sdklist-info li{ clear: both; overflow: hidden; padding:10px 0; border:none;}
	label.lab-left{ width: 100px; line-height: 26px; font-size: 12px;}
	input[type="text"]{ line-height: 26px; width: 260px; border: 1px solid #ccc;}
	textarea{ width: 610px; resize:none; height: 120px; line-height: 24px; overflow: auto; border: 1px solid #ccc;}
	input[type="checkbox"]+label{ position: relative; top: -2px; padding-left: 5px;}
	.save-btn{ background: #298BB8; border-radius: 5px; font-size: 18px; padding: 10px 60px; color: #FFF;}
	.save-btn:hover{ background: #298BEF; text-decoration: none;}
	select{ line-height: 26px; height: 26px; width: 120px;}
	.input-file{ position: absolute; left:0; left: 0px;opacity:0; filter:alpha(opacity=0); line-height: 28px; height:28px; width:230px; cursor:pointer;}
	.browse-btn{line-height: 28px;padding: 0 15px; background: #808080; color: #FFF;text-align: center; height:28px;}
	.upload-btn{ line-height: 28px;padding: 0 15px; background: #49c39d; color: #FFF;text-align: center; border:0; cursor:pointer;height:28px;}
	a:hover{text-decoration: none;}
	input[type="text"].textfield{width: 174px;}
	fieldset{border:1px solid #a6c9e2;}
	.filedset-pad{padding:10px 5px 0;}
	.area01{margin:10px 0 0 300px;}
</style>

<script type="text/javascript">

</script>


</head>
<body>
<form id="update_info_form" method="post" class="form">
		<input type="hidden" name="id" value="${crawl.id}"/>
		<div class="filedset-pad">
		<fieldset>
    	<legend><font style="color:red">爬虫详细设置</font></legend>
		<ul class="sdklist-info">
			<li>
				<label class="fl lab-left">listUrl：</label> 
				<input type="text" style="width:632px" maxLength="100" class="fl" id="listUrl" name="listUrl" value="${crawl.listUrl}" readonly>
			</li>
			
			<li>
				<label class="fl lab-left">start：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="start" name="start" value="${crawl.start}">
				<label class="fl lab-left" style="margin-left:132px;">end：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="end" name="end" value="${crawl.end}">
			</li>
			
			<li>
				<label class="fl lab-left">infoUrlXpath：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoUrlXpath" name="infoUrlXpath" value="${crawl.infoUrlXpath}" readonly>
				<label class="fl lab-left" style="margin-left:132px;">listUrlRegular：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="listUrlRegular" name="listUrlRegular" value="${crawl.listUrlRegular}" readonly>
			</li>
			
			<li>
				<label class="fl lab-left">infoUrlPre：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoUrlPre" name="infoUrlPre" value="${crawl.infoUrlPre}" readonly>
				<label class="fl lab-left" style="margin-left:132px;">infoUrlRegular：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoUrlRegular" name="infoUrlRegular" value="${crawl.infoUrlRegular}" readonly>
			</li>
			
			<li>
				<label class="fl lab-left">infoContentXpath：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoContentXpath" name="infoContentXpath" value="${crawl.infoContentXpath}" readonly>
				<label class="fl lab-left" style="margin-left:132px;">infoPicXpath：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoPicXpath" name="infoPicXpath" value="${crawl.infoPicXpath}" readonly>
			</li>
			
			<li>
				<label class="fl lab-left">repeatFlagXpath：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="repeatFlagXpath" name="repeatFlagXpath" value="${crawl.repeatFlagXpath}" readonly>
				<label class="fl lab-left" style="margin-left:132px;">repeatMaxTimes：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="repeatMaxTimes" name="repeatMaxTimes" value="${crawl.repeatMaxTimes}">
			</li>
			
		</ul>
		</fieldset>
		</div>
		<div class="filedset-pad">
		<fieldset>
    	<legend><font style="color:red">爬虫定时策略</font></legend>
		<ul class="sdklist-info">
			<li>
				<label class="fl lab-left">周期：</label> 
				<select id="crawlTime" name="crawlTime" style="width:200px">
					<option value="1"
					<c:if test="${crawl.crawlTime == 1}">
						selected
					</c:if>
					>1</option>
					<option value="2"
					<c:if test="${crawl.crawlTime == 2}">
						selected
					</c:if>
					>2</option>
					<option value="3"
					<c:if test="${crawl.crawlTime == 3}">
						selected
					</c:if>
					>3</option>
					<option value="4"
					<c:if test="${crawl.crawlTime == 4}">
						selected
					</c:if>
					>4</option>
					<option value="5"
					<c:if test="${crawl.crawlTime == 5}">
						selected
					</c:if>
					>5</option>
					<option value="6"
					<c:if test="${crawl.crawlTime == 6}">
						selected
					</c:if>
					>6</option>
					<option value="7"
					<c:if test="${crawl.crawlTime == 7}">
						selected
					</c:if>
					>7</option>
					<option value="8"
					<c:if test="${crawl.crawlTime == 8}">
						selected
					</c:if>
					>8</option>
					<option value="9"
					<c:if test="${crawl.crawlTime == 9}">
						selected
					</c:if>
					>9</option>
					<option value="10"
					<c:if test="${crawl.crawlTime == 10}">
						selected
					</c:if>
					>10</option>
					<option value="11"
					<c:if test="${crawl.crawlTime == 11}">
						selected
					</c:if>
					>11</option>
					<option value="12"
					<c:if test="${crawl.crawlTime == 12}">
						selected
					</c:if>
					>12</option>
					<option value="13"
					<c:if test="${crawl.crawlTime == 13}">
						selected
					</c:if>
					>13</option>
					<option value="14"
					<c:if test="${crawl.crawlTime == 14}">
						selected
					</c:if>
					>14</option>
					<option value="15"
					<c:if test="${crawl.crawlTime == 15}">
						selected
					</c:if>
					>15</option>
					<option value="16"
					<c:if test="${crawl.crawlTime == 16}">
						selected
					</c:if>
					>16</option>
					<option value="17"
					<c:if test="${crawl.crawlTime == 17}">
						selected
					</c:if>
					>17</option>
					<option value="18"
					<c:if test="${crawl.crawlTime == 18}">
						selected
					</c:if>
					>18</option>
					<option value="19"
					<c:if test="${crawl.crawlTime == 19}">
						selected
					</c:if>
					>19</option>
					<option value="20"
					<c:if test="${crawl.crawlTime == 20}">
						selected
					</c:if>
					>20</option>
					<option value="21"
					<c:if test="${crawl.crawlTime == 21}">
						selected
					</c:if>
					>21</option>
					<option value="22"
					<c:if test="${crawl.crawlTime == 22}">
						selected
					</c:if>
					>22</option>
					<option value="23"
					<c:if test="${crawl.crawlTime == 23}">
						selected
					</c:if>
					>23</option>
					<option value="24"
					<c:if test="${crawl.crawlTime == 24}">
						selected
					</c:if>
					>24</option>
				</select>
				--
				<select style="width:200px">
					<option value="1">点</option>
				</select>
				<font style="color:red">(多个爬虫的爬取时间建议不要重叠,防止带宽不足抓取失败)</font>
			</li>
			
			<li>
				<label class="fl lab-left">休眠时间：</label> 
				<select name="sleepTime" id="sleepTime" style="width:200px">
					<option value="100" 
					<c:if test="${crawl.sleepTime == 100}">
						selected
					</c:if>
					>100毫秒</option>
					<option value="200"
					<c:if test="${crawl.sleepTime == 200}">
						selected
					</c:if>
					>200毫秒</option>
					<option value="300"
					<c:if test="${crawl.sleepTime == 300}">
						selected
					</c:if>
					>300毫秒</option>
					<option value="400"
					<c:if test="${crawl.sleepTime == 400}">
						selected
					</c:if>
					>400毫秒</option>
					<option value="500"
					<c:if test="${crawl.sleepTime == 500}">
						selected
					</c:if>
					>500毫秒</option>
					<option value="600"
					<c:if test="${crawl.sleepTime == 600}">
						selected
					</c:if>
					>600毫秒</option>
					<option value="700"
					<c:if test="${crawl.sleepTime == 700}">
						selected
					</c:if>
					>700毫秒</option>
					<option value="800"
					<c:if test="${crawl.sleepTime == 800}">
						selected
					</c:if>
					>800毫秒</option>
					<option value="900"
					<c:if test="${crawl.sleepTime == 900}">
						selected
					</c:if>
					>900毫秒</option>
					<option value="1000"
					<c:if test="${crawl.sleepTime == 1000}">
						selected
					</c:if>
					>1000毫秒</option>
				</select>
				<font style="color:red">(不同网站抓取休眠时间不同，防止请求过快抓取失败)</font>
			</li>	
		</ul>
		</fieldset>
	</div>
</form>
</body>
</html>
