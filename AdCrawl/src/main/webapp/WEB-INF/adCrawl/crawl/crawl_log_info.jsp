<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
	body,div,input,label,h1,ul,li{margin: 0; padding: 0;}
	body, button, input, select, textarea {font: 12px "微软雅黑",arial,sans-serif;color:#2e2e2e;}
	table {border-collapse:collapse;border-spacing:0;}
	fieldset,img {border:0}
	address,caption,cite,code,dfn,strong,th,var{font-style:normal;font-weight:normal}
	ol,ul {list-style:none}
	caption,th {text-align:left}
	h1,h2,h3,h4,h5,h6 {font-size:100%;font-weight:normal}
	q:before,q:after {content:''}
	abbr,acronym { border:0}
	a{text-decoration:none;color:#999;outline:none;hide-focus:expression(this.hideFocus=true);}
	em{font-style:normal;}
	input,a,textarea{ outline:none;}
	a:hover{ text-decoration:underline;}
	html,body{width:100%;height:100%;}
	.clearfix:after{content:".";display:block;height:0;clear:both;visibility:hidden}
	.clearfix{display:inline-block}* html .clearfix{height:1%}.clearfix{display:block}
	.fl{float:left;_display:inline}
	.p20{ padding: 20px;}.pt15{padding-top: 15px;}.pl20{padding-left: 20px;}.pb10{padding-bottom: 10px;}
	.m20{margin-left: 10px;}.ml10{margin-left:10px;}.mt20{margin-top:20px;}
	.pr{position: relative;}
	.h1-title{ font-size: 24px; line-height: 50px; font-weight: normal;}
	.sdklist-info li{ clear: both; overflow: hidden; padding:10px 0; border:none;}
	label.lab-left{ width: 100px; line-height: 26px; font-size: 12px;}
	input[type="text"]{ line-height: 26px; width: 260px; border: 1px solid #ccc;}
	textarea{ width: 610px; resize:none; height: 120px; line-height: 24px; overflow: auto; border: 1px solid #ccc;}
	input[type="checkbox"]+label{ position: relative; top: -2px; padding-left: 5px;}
	.save-btn{ background: #298BB8; border-radius: 5px; font-size: 18px; padding: 10px 60px; color: #FFF;}
	.save-btn:hover{ background: #298BEF; text-decoration: none;}
	select{ line-height: 26px; height: 26px; width: 120px;}
	.input-file{ position: absolute; left:0; left: 0px;opacity:0; filter:alpha(opacity=0); line-height: 28px; height:28px; width:230px; cursor:pointer;}
	.browse-btn{line-height: 28px;padding: 0 15px; background: #808080; color: #FFF;text-align: center; height:28px;}
	.upload-btn{ line-height: 28px;padding: 0 15px; background: #49c39d; color: #FFF;text-align: center; border:0; cursor:pointer;height:28px;}
	a:hover{text-decoration: none;}
	input[type="text"].textfield{width: 174px;}
	fieldset{border:1px solid #a6c9e2; margin-top:10px;}
	.filedset-pad{padding:0 5px;}
	.area01{margin:10px 0 0 300px;}
	.fr{float:right;}
	.log-left{ width:715px; word-wrap:break-word; line-height:22px; height:345px; overflow:auto; padding:5px 0;}
	.log-right{width:340px;border-left:1px solid #a6c9e2;  height:355px; margin:0px 8px; padding:10px;}
	.log-time{ padding:3px 10px; background:#298bb8; color:#FFF; border-radius:5px;}
	.log-con{margin:5px 0; text-indent:2em;}
</style>

<script type="text/javascript">

setInterval(function(){
	$.ajax({
         type: "post",
         url: "getIncrCrawlLogById",
         data: {
         	id : $("#id").val()
         },
         dataType: "json",
         success: function(data){
	         var result = eval(data);
	         $("#crawlInfo").prepend(result.data);
         }
    });
},1000); 

</script>


</head>
<body>
<form id="sdk_info_form" method="post" class="form">
	<input type="hidden"  id="id" name="id" value="${id}"/>
	<div class="filedset-pad">
 		<fieldset>
    	<legend><font style="color:red">日志</font></legend>
    		<div class="clearfix">
				<div id="crawlInfo" class="log-left fl">
					<c:forEach items="${logList}" var="log">
						<span class="log-time">${log.time}</span>
						<div class="log-con">
							${log.log}
						</div>
					</c:forEach>
				</div>
				<div class="log-right fr">
					<ul class="sdklist-info">
						<li>
							<label class="fl lab-left">列表地址模板：</label> 
							<input type="text" style="width:200px" maxLength="100" class="fl" id="listUrl" name="listUrl" value="${crawl.listUrl}" readonly>
						</li>
			
						<li>
							<label class="fl lab-left">开始页：</label> 
							<input type="text" style="width:200px" maxLength="100" class="fl" id="start" name="start" value="${crawl.start}" readonly>
						</li>
						<li>
							<label class="fl lab-left">结束页：</label> 
							<input type="text" style="width:200px" maxLength="100" class="fl" id="end" name="end" value="${crawl.end}" readonly>
						</li>
						<li>
							<label class="fl lab-left">内容模板：</label> 
							<input type="text" style="width:200px" maxLength="100" class="fl" id="start" name="repeatMaxtimes" value="${crawl.infoContentXpath}" readonly>
						</li>
						<li>
							<label class="fl lab-left">图片模板：</label> 
							<input type="text" style="width:200px" maxLength="100" class="fl" id="start" name="repeatMaxtimes" value="${crawl.infoPicXpath}" readonly>
						</li>
						<li>
							<label class="fl lab-left">重复上限：</label> 
							<input type="text" style="width:200px" maxLength="100" class="fl" id="start" name="repeatMaxtimes" value="${crawl.repeatMaxTimes}" readonly>
						</li>
						<li>
							<label class="fl lab-left">休眠时间：</label> 
							<input type="text" style="width:200px" maxLength="100" class="fl" id="end" name="end" value="${crawl.sleepTime}ms" readonly>
						</li>
					</ul>
				</div>
			</div>
		</fieldset>
		
	</div>
</form>
</body>
</html>
