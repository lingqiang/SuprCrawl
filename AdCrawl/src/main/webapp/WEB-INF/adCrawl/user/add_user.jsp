<%@ page language="java" import="java.util.*"  pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<meta name="MobileOptimized" content="240" />
	<meta name="apple-touch-fullscreen" content="YES" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<script type="text/javascript" src="<%=basePath%>js/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<style>
	/* BASE CSS */
	body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote,th,td {margin:0;padding:0;}
	body, button, input, select, textarea {font: 12px/1.5 "冬青黑",arial,sans-serif;color:#2e2e2e; font-size: 18px;}
	table {border-collapse:collapse;border-spacing:0;}
	fieldset,img {border:0}
	address,caption,cite,code,dfn,strong,th,var{font-style:normal;font-weight:normal}
	ol,ul {list-style:none}
	caption,th {text-align:left}
	h1,h2,h3,h4,h5,h6 {font-size:100%;font-weight:normal}
	q:before,q:after {content:''}
	abbr,acronym { border:0}
	a{text-decoration:none;color:#FFF;outline:none;hide-focus:expression(this.hideFocus=true);}
	em{font-style:normal;}
	input,a,textarea{ outline:none;}
	html,body{width:100%;height:100%;}
	body{background-color: #f5f5f5;}
	.fl{float:left;_display:inline} .fm{float:left;_display:inline;} .fr{float:right;_display:inline}
	span.fl,span.fm,span.fr{display:block;_display:inline;}
	.cb{clear:both} .cl{clear:left} .cr{clear:right}.tr{text-align: right;}
	.clearfix:after{content:".";display:block;height:0;clear:both;visibility:hidden}
	.clearfix{display:inline-block}* html .clearfix{height:1%}.clearfix{display:block}
	.clear{width:100%;height:1px;overflow:hidden;clear:both;}
	.f12{font-size:12px} .f13{font-size:13px} .f14{font-size:14px;} .f16{font-size:16px} .f18{font-size:18px} .f20{font-size:20px}
	.f24{font-size:24px;}.f30{font-size:30px;}.f15{font-size: 15px;}
	img{ vertical-align: middle;}
	.fff{ color: #FFF;}.tc{text-align: center;}
	.mt30{margin-top: 30px;}.mt15{margin-top: 15px;}.ml10{ margin-left: 10px;}.ml40{margin-left: 40px;}.ml20{margin-left: 20px;}.pb30{ padding-bottom: 30px;}
	.mt10{ margin-top: 10px;}.pt30{ padding-top: 30px;}.ml5{margin-left: 5px;}.pt10{ padding-top: 10px;}.pt20{ padding-top: 20px;}.ml{margin-left: 20px;}
	.mt5{ margin-top: 5px;}
	.demo-header{ background-color: #363636; height: 60px; padding: 0 5%;}
	.demo-header h1{ font-size: 16px; color: #fff; text-align: center; padding-top: 15px; line-height:30px; height: 30px; white-space: nowrap; text-overflow:ellipsis; overflow: hidden; }
	.demo-contain{ padding: 10px 5%;}
	.form-list{margin-top: 40px;}
	.form-list li{ position: relative; padding-bottom: 40px;}
	.lab-com{ width: 60px; line-height: 40px; height: 40px; position: absolute; left: 0px; top: 0px;}
	input[type="text"],select{ line-height: 40px; height: 40px; margin-left: 60px;}
	.submit{line-height: 45px; height: 40px; margin-left: 60px; }
</style>


<script type="text/javascript">	

$(function(){
	setWidth();
	$(window).resize(function(){
	    setWidth();
	});

})
var setWidth = function(){
		var liW= $(".form-list li").width();
		$("input[type='text'],select,.submit").css("width",liW-60);
	};


function addUser(){
	
	var param = {};
	
	if($("#userName").val() == null || $("#userName").val() == ''){
		alert("请输入用户名称");
		return false;
	}
	
	if($("#mobile").val() == null || $("#mobile").val() == ''){
		alert("请输入用户手机号码");
		return false;
	}
	
	param["userName"] = $("#userName").val();
	param["mobile"] = $("#mobile").val();
	param["trade"] = $("#trade").val();
	
	$.ajax({
        type : "POST",
        url : 'addUsers',
        data : 'param='+JSON.stringify(param),
        success  : function(data) {
        	if(data.resultCode == 'success'){
        		alert("新增成功");
        	}else{
        		alert("新增失败");
        	}
		}
    });	
}

</script>

<body>   
	
	<div class="demo-header">
		<h1>填写信息</h1>
	</div>
	<div class="demo-contain">
		<ul class="form-list">
			<li>
				<label class="lab-com fl">姓名：</label><input type="text" name="userName" id="userName" placeholder="请输入您的姓名">
			</li>
			<li>
				<label class="lab-com fl">手机：</label><input type="text" name="mobile" id="mobile" placeholder="请输入您的手机号码">
			</li>
			<li>
				<label class="lab-com fl">行业：</label>
				<select name="trade" id="trade">
					<option value="1">IT</option>
					<option value="2">金融</option>
					<option value="3">教育</option>
				</select>
			</li>
			<li><label class="lab-com fl">&nbsp;</label><input onclick="addUser()" type="submit" value="提交" class="submit"></li>
		</ul>
	</div>
</body>  
</html>
