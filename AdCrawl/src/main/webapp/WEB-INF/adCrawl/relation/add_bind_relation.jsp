<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加客户</title>
<script type="text/javascript">
					
function changeMenu(){
	var busId = $("#menuNameId");
	 empty(busId);
 
 	var parentId = $("#parentId").val();
	$.ajax({
          type: "post",
          url: "getMenuListById",
          data: {
        	  parentId : parentId
          },
          dataType: "json",
          success: function(data){
	         var result = eval(data);
	         $('#menuNameId').html(result.data);
           }
      }); 
	
}

function changeCategory(){
	var busId = $("#site_category_id");
	 empty(busId);
 
 	var siteId = $("#site_id").val();
	$.ajax({
          type: "post",
          url: "getSiteCategoryById",
          data: {
        	  siteId : siteId
          },
          dataType: "json",
          success: function(data){
	         var result = eval(data);
	         $('#site_category_id').html(result.data);
           }
      }); 
	
}

function empty(id) {
	id.empty();
	id.append("<option value='0'>请选择</option>");
}

function validateSave(){
	
	var parentId = $("#parentId").val();
	var menuNameId = $("#menuNameId").val();
	var siteId = $("#site_id").val();
	var siteCategoryId = $("#site_category_id").val();
	
	if(parentId==0){
		Ext.example.msg('', '请选择父类栏目', '');
		return false;
	}
	if(menuNameId==0){
		Ext.example.msg('', '请选择子类栏目', '');
		return false;
	}
	if(siteId==0){
		Ext.example.msg('', '请选择站点名称', '');
		return false;
	}
	if(siteCategoryId==0){
		Ext.example.msg('', '请选择分类名称', '');
		return false;
	}
	return true;
}

</script>
	
</head>
<body>
<form id="add_bind_relation_form" method="post" class="form" action=""  >
	<div class="filedset-pad">
 		<fieldset>
    	<legend>绑定关系</legend>
		<ul class="sdklist-info">
			<li>
				<label for="" class="fl lab-left">父类栏目：</label>
				<select name="parentId" id="parentId" class="fl" style="width:180px" onchange="changeMenu()">
					<option value="0">请选择</option>
					<c:forEach items="${menuList}" var="menu">
						<option value="${menu.id}">${menu.menuName}</option>
					</c:forEach>
				</select>
			</li>  
			<li>
				<label for="" class="fl lab-left">子类栏目：</label>
				<select name="menuId" id="menuNameId"  class="fl" style="width:180px" >
					<option value="0">请选择</option>
				</select>
			</li>  
			
			<li>
				<label for="" class="fl lab-left">站点名称：</label>
				<select name="siteId" id="site_id"  class="fl" style="width:180px" onchange="changeCategory()">
					<option value="0">请选择</option>
					<c:forEach items="${siteList}" var="site">
						<option value="${site.id}">${site.siteName}</option>
					</c:forEach>
				</select>
			</li>  
			<li>
				<label for="" class="fl lab-left">分类名称：</label>
				<select name="siteCategoryId" id="site_category_id" class="fl" style="width:180px" >
					<option value="0">请选择</option>
				</select>
			</li>  
			
		</ul>
		</fieldset>
	</div>
</form>


</body>


</html>
