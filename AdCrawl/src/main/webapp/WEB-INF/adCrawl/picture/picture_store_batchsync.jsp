<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>同步菜单</title>

<style>
	body,div,input,label,h1,ul,li{margin: 0; padding: 0;}
	body, button, input, select, textarea {font: 12px "微软雅黑",arial,sans-serif;color:#2e2e2e;}
	table {border-collapse:collapse;border-spacing:0;}
	fieldset,img {border:0}
	address,caption,cite,code,dfn,strong,th,var{font-style:normal;font-weight:normal}
	ol,ul {list-style:none}
	caption,th {text-align:left}
	h1,h2,h3,h4,h5,h6 {font-size:100%;font-weight:normal}
	q:before,q:after {content:''}
	abbr,acronym { border:0}
	a{text-decoration:none;color:#999;outline:none;hide-focus:expression(this.hideFocus=true);}
	em{font-style:normal;}
	input,a,textarea{ outline:none;}
	a:hover{ text-decoration:underline;}
	html,body{width:100%;height:100%;}
	.clearfix:after{content:".";display:block;height:0;clear:both;visibility:hidden}
	.clearfix{display:inline-block}* html .clearfix{height:1%}.clearfix{display:block}
	.fl{float:left;_display:inline}
	.p20{ padding: 20px;}.pt15{padding-top: 15px;}.pl20{padding-left: 20px;}.pb10{padding-bottom: 10px;}
	.m20{margin-left: 10px;}.ml10{margin-left:10px;}.mt20{margin-top:20px;}
	.pr{position: relative;}
	.h1-title{ font-size: 24px; line-height: 50px; font-weight: normal;}
	.sdklist-info li{ clear: both; overflow: hidden; padding:10px 0;}
	label.lab-left{ width: 100px; line-height: 26px; font-size: 12px;}
	input[type="text"]{ line-height: 26px; width: 260px; border: 1px solid #ccc;}
	textarea{ width: 560px; resize:none; height: 120px; line-height: 24px; overflow: auto; border: 1px solid #ccc;}
	input[type="checkbox"]+label{ position: relative; top: -2px; padding-left: 5px;}
	.save-btn{ background: #49c39d; border-radius: 5px; font-size: 18px; padding: 10px 60px; color: #FFF;}
	.save-btn:hover{ background: #6ce6c0; text-decoration: none;}
	select{ line-height: 26px; height: 26px; width: 120px;}
	.input-file{ position: absolute; left:0; left: 0px;opacity:0; filter:alpha(opacity=0); line-height: 28px; height:28px; width:230px; cursor:pointer;}
	.browse-btn{line-height: 28px;padding: 0 15px; background: #808080; color: #FFF;text-align: center; height:28px;}
	.upload-btn{ line-height: 28px;padding: 0 15px; background: #49c39d; color: #FFF;text-align: center; border:0; cursor:pointer;height:28px;}
	a:hover{text-decoration: none;}
	input[type="text"].textfield{width: 174px;}
	fieldset{border:1px solid #a6c9e2; margin-top:10px;}
	fieldset p{ text-indent:2em;line-height:22px; margin:0; padding:0;}
	.filedset-pad{padding:0 5px;}
	.img-con{text-align:center;}
	.img-con img{ padding-bottom:10px; max-width:450px !important; width:auto;}
	.btn-blue{line-height:28px;padding:0 15px; color:#FFF; background:#298BB8;}
	.high{ color:red; } 
	.select_border{height:27px;*background:#fff;background:#fff;*border:1px solid #FFFFFF;*padding:4px;*height:16px;}
	.container{*border:0;*position:relative;*height:18px;*overflow:hidden;}
	.select{border:1px solid #ccc;padding:1px 3px;font-size:12px;height:29px;margin:-1px; position: relative; *top: -9px;}
	
</style>

</head>
<body>
<form id="update_info_form" method="post" class="form" action=""  enctype="multipart/form-data" target="apkIframe">
	<div class="filedset-pad">
    	<fieldset>
    	<legend>同步菜单</legend>
			<div class="select_border fl" style="width: 159px; *width:150px; margin:0 10px;">
                 <div class="container" style="*width:150px;">
                     <select id="menuId" name="" class="select" style="width: 200px;"> 
                         <c:forEach items="${menuList}" var="menu">
							<option value="${menu.id}">${menu.menuName}</option>
						</c:forEach>
		        	</select>
                 </div>
            </div>
 		</fieldset>
	</div>
</form>

</body>

</html>
