<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=basePath%>css/public.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>app/extend/example.css" type="text/css"></link>
<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=basePath%>js/ajaxfileupload.js"></script>
<script type="text/javascript" src="<%=basePath%>js/ext-4.2/ext-all.js"></script>
<script type="text/javascript" src="<%=basePath%>js/ext-4.2/locale/ext-lang-zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>app/util/common.js"></script>
<script type="text/javascript" src="<%=basePath%>app/extend/examples.js"></script>
<style>
	body,div,input,label,h1,ul,li{margin: 0; padding: 0;}
	body, button, input, select, textarea {font: 12px "微软雅黑",arial,sans-serif;color:#2e2e2e;}
	table {border-collapse:collapse;border-spacing:0;}
	fieldset,img {border:0}
	address,caption,cite,code,dfn,strong,th,var{font-style:normal;font-weight:normal}
	ol,ul {list-style:none}
	caption,th {text-align:left}
	h1,h2,h3,h4,h5,h6 {font-size:100%;font-weight:normal}
	q:before,q:after {content:''}
	abbr,acronym { border:0}
	a{text-decoration:none;color:#999;outline:none;hide-focus:expression(this.hideFocus=true);}
	em{font-style:normal;}
	input,a,textarea{ outline:none;}
	a:hover{ text-decoration:underline;}
	html,body{width:100%;height:100%;}
	.clearfix:after{content:".";display:block;height:0;clear:both;visibility:hidden}
	.clearfix{display:inline-block}* html .clearfix{height:1%}.clearfix{display:block}
	.fl{float:left;_display:inline}
	.p20{ padding: 20px;}.pt15{padding-top: 15px;}.pl20{padding-left: 20px;}.pb10{padding-bottom: 10px;}
	.m20{margin-left: 10px;}.ml10{margin-left:10px;}.mt20{margin-top:20px;}
	.pr{position: relative;}
	.h1-title{ font-size: 24px; line-height: 50px; font-weight: normal;}
	.sdklist-info li{ clear: both; overflow: hidden; padding:10px 0; border:none;}
	label.lab-left{ width: 100px; line-height: 26px; font-size: 12px;}
	input[type="text"]{ line-height: 26px; width: 260px; border: 1px solid #ccc;}
	textarea{ width: 610px; resize:none; height: 120px; line-height: 24px; overflow: auto; border: 1px solid #ccc;}
	input[type="checkbox"]+label{ position: relative; top: -2px; padding-left: 5px;}
	.save-btn{ background: #298BB8; border-radius: 5px; font-size: 18px; padding: 10px 60px; color: #FFF;}
	.save-btn:hover{ background: #298BEF; text-decoration: none;}
	select{ line-height: 26px; height: 26px; width: 120px;}
	.input-file{ position: absolute; left:0; left: 0px;opacity:0; filter:alpha(opacity=0); line-height: 28px; height:28px; width:230px; cursor:pointer;}
	.browse-btn{line-height: 28px;padding: 0 15px; background: #808080; color: #FFF;text-align: center; height:28px;}
	.upload-btn{ line-height: 28px;padding: 0 15px; background: #49c39d; color: #FFF;text-align: center; border:0; cursor:pointer;height:28px;}
	a:hover{text-decoration: none;}
	input[type="text"].textfield{width: 174px;}
	fieldset{border:1px solid #a6c9e2;}
	.filedset-pad{padding:10px 5px 0;}
	.area01{margin:10px 0 0 300px;}
	.file-pa{ position:absolute; left:0px; top:0px; width:68px; height:28px;cursor:pointer;}
	.img-conbox{ position:absolute; right:40px; top:10px; width:340px; height:260px; border:1px solid #a6c9e2;}
	.img-conbox img{ width:100%;}
</style>

</head>
<body>
<form id="sdk_info_form" method="post" class="form">
	<div class="filedset-pad">
 		<fieldset>
    	<legend><font style="color:red">第一步、画作级别</font></legend>
		<ul class="sdklist-info">
			<li>
				<label class="fl lab-left">画作级别：</label>
				<select id="level" name="level" style="width:200px">
					<option value="1">所有人群</option>
					<option value="2">18岁以上人群</option>
					<option value="3">限制级别</option>
				</select>
			</li>
		</ul>
		</fieldset>
		</div>
		<div class="filedset-pad">
		<fieldset>
    	<legend><font style="color:red">第二步、画作信息</font></legend>
    	<div style="position:relative">
		<ul class="sdklist-info">
			<li>
				<label class="fl lab-left">画作名称：</label> 
				<input type="text" style="width:612px" maxLength="100" class="fl" id="paintName" name="paintName">
			</li>
			
			<li>
				<label class="fl lab-left">画家姓名：</label> 
				<input type="text" style="width:206px" maxLength="100" class="fl" id="paintist" name="paintist">
				<label class="fl lab-left" style="margin-left:100px;">国籍：</label> 
				<input type="text" style="width:206px" maxLength="100" class="fl" id="nationality" name="nationality">
			</li>
			
			<li>
				<label class="fl lab-left">生年：</label> 
				<input type="text" style="width:206px" maxLength="100" class="fl" id="birthYear" name="birthYear">
				<label class="fl lab-left" style="margin-left:100px;">卒年：</label> 
				<input type="text" style="width:206px" maxLength="100" class="fl" id="deathYear" name="deathYear">
			</li>
			
			<li>
				<label class="fl lab-left">画作简介：</label> 
				<textarea rows="3" cols="20" id="paintDesc" name="paintDesc" maxlength="500"></textarea>
			</li>
			
			<li>
				<label class="fl lab-left">画作链接地址：</label> 
				<input name="paintUrl" id="paintUrl" type="text" style="width:612px" readonly class="fl">
				<div class="fl pr" stye="width:68px; height:28px; margin-left:10px;">
					<a href="javascript:void(0)" class="save-btn" style="margin-left:8px;display:block;padding: 4px 20px;font-size: 14px;">上传</a>
					<input id="paintFile" name="paintFile" type="file" class="input-file file-pa" style="background: #298BB8;" onchange="upload()"/>
				</div>
			</li>
			
		</ul>
		<div class="img-conbox"><img id="showPic" src="" style="display:none;"/></div>
		</div>
		</fieldset>
		</div>
		
		<div class="filedset-pad">
			<a href="javascript:void(0)" id="save-btn" class="save-btn fl area01" onclick="addPaint()">保存</a>
		</div>
</form>
</body>

<script type="text/javascript">

$(function() {
	$(".upload-btn").click(function(){
		$(this).siblings(".input-file").click();
	});

});

// 文件上传
function upload(){
		var file = document.getElementById("paintFile").value;
		if (file == undefined || file == '') {
			Ext.example.msg('','请选择需要上传的文件', '');
			return;
		} else if (!/.(png)$/.test(file) && !/.(jpg)$/.test(file)) {
			Ext.example.msg('','文件类型必须是.png或者.jpg', '');
			return;
		} else {
			$.ajaxFileUpload({
				url : './upload',
				secureuri : false,
				method : 'post',
				fileElementId : 'paintFile',
				dataType : 'json',
				data : {}, // 其它请求参数  
				success : function(data, status) {
					$("#paintUrl").val(getRootPath() + data.msg);
					$("#showPic").attr("src",getRootPath() + data.msg);
					$("#showPic").show();
				},
				error : function(data, status, e) {
					Ext.example.msg('','上传失败', '');
				}
			})
		}
}

function getRootPath() {
		//获取当前网址，如： http://localhost:8080/GameFngine/share/meun.jsp
		var curWwwPath = window.document.location.href;
		//获取主机地址之后的目录，如： GameFngine/meun.jsp
		var pathName = window.document.location.pathname;
		var pos = curWwwPath.indexOf(pathName);
		//获取主机地址，如： http://localhost:8080
		var localhostPaht = curWwwPath.substring(0, pos);
		//获取带"/"的项目名，如：/GameFngine
		var projectName = pathName.substring(0,
				pathName.substr(1).indexOf('/') + 1);
		return (localhostPaht + projectName + "/uploads/");
}

// 保存名画
function addPaint(){
	if(validate()){
		// 新增按钮置为不可用
		$("#save-btn").attr("onclick", "#");
		$.ajax({
			type : "POST",
			url : './savePaint',
			data : decodeURIComponent($("#sdk_info_form").serialize(),true),
			success : function(data) {
				if(data.resultCode == 'success'){
					Ext.example.msg('','新增成功', '');	
					// 刷新页面
					setTimeout(function(){
                    	window.location.reload();
   					}, 1000);
				}else{
					$("#save-btn").attr("onclick", "addPaint()");
					Ext.example.msg('','新增失败', '');
				}
			}
		});
		
	}
}

// 校验数据
function validate(){
	var paintName = $.trim($("#paintName").val());
	var paintist = $.trim($("#paintist").val());
	var nationality = $.trim($("#nationality").val());
	var birthYear = $.trim($("#birthYear").val());
	var deathYear = $.trim($("#deathYear").val());
	var paintDesc = $.trim($("#paintDesc").val());
	var paintUrl = $.trim($("#paintUrl").val());
	
	if(isEmpty(paintName)){
		Ext.example.msg('','画作名称不能为空', '');	
		return false;
	}
	if(isEmpty(paintist)){
		Ext.example.msg('','画家姓名不能为空', '');	
		return false;
	}
	if(isEmpty(nationality)){
		Ext.example.msg('','国籍不能为空', '');	
		return false;
	}
	if(isEmpty(birthYear)){
		Ext.example.msg('','生年不能为空', '');	
		return false;
	}
	if(isEmpty(deathYear)){
		Ext.example.msg('','卒年不能为空', '');	
		return false;
	}
	if(isEmpty(paintDesc)){
		Ext.example.msg('','画作简介不能为空', '');	
		return false;
	}
	if(isEmpty(paintUrl)){
		Ext.example.msg('','画作链接地址不能为空', '');	
		return false;
	}

	return true;
}

// 判断是否为空
function isEmpty(str){
	if(str != 'undefined' && str != null && $.trim(str) != ''){
		return false;
	}
	return true;
}

</script>
</html>
