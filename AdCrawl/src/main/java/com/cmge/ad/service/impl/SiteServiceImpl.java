package com.cmge.ad.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.cmge.ad.mapper.SiteMapper;
import com.cmge.ad.model.Menu;
import com.cmge.ad.model.Site;
import com.cmge.ad.model.SiteCategory;
import com.cmge.ad.service.SiteService;
import com.cmge.ad.util.Pager;

@Service
@Transactional(isolation=Isolation.DEFAULT,rollbackFor=Exception.class)
public class SiteServiceImpl implements SiteService{
	
	@Autowired
	private SiteMapper siteMapper;
	
	@Override
	public List<Site> getAllSiteList(int type) {
		return siteMapper.getAllSiteList(type);
	}
	
	@Override
	public List<Menu> getParentMenu() {
		return siteMapper.getParentMenu();
	}
	
	@Override
	public void deleteChildMenuById(Integer id) {
		siteMapper.deleteChildMenuById(id);
	}
	
	@Override
	public boolean existMenuRelation(Integer id) {
		return siteMapper.existMenuRelation(id) > 0 ? true : false;
	}
	
	@Override
	public List<SiteCategory> getCategoryBySiteId(String siteId) {
		return siteMapper.getCategoryBySiteId(siteId);
	}
	
	@Override
	public void getSiteList(Pager<Site> pager, HashMap<String, Object> paramMap) {
		int count = siteMapper.getSiteCount(paramMap);
		pager.setTotalCount(count);
		if(count > 0){
			paramMap.put("start", pager.getStart());
			paramMap.put("limit", pager.getLimit());
			List<Site> appList = siteMapper.getSiteList(paramMap);
			pager.setRows(appList);
		}
	}
	
	@Override
	public void getSiteCategoryList(Pager<SiteCategory> pager, HashMap<String, Object> paramMap) {
		int count = siteMapper.getSiteCategoryCount(paramMap);
		pager.setTotalCount(count);
		if(count > 0){
			paramMap.put("start", pager.getStart());
			paramMap.put("limit", pager.getLimit());
			List<SiteCategory> appList = siteMapper.getSiteCategoryList(paramMap);
			pager.setRows(appList);
		}
	}
	
	@Override
	public void getMenuList(Pager<Menu> pager, HashMap<String, Object> paramMap) {
		int count = siteMapper.getMenuCount(paramMap);
		pager.setTotalCount(count);
		if(count > 0){
			paramMap.put("start", pager.getStart());
			paramMap.put("limit", pager.getLimit());
			List<Menu> appList = siteMapper.getMenuList(paramMap);
			pager.setRows(appList);
		}
	}
	
	@Override
	public void addChildMenu(Menu menu) {
		siteMapper.addChildMenu(menu);
	}
	
	@Override
	public boolean existMenu(Menu menu) {
		return siteMapper.existMenu(menu) > 0 ? true : false;
	}

	@Override
	public List<Site> getAllSiteInfoList() {
		return siteMapper.getAllSiteInfoList();
	}

	@Override
	public List<Menu> getMenuListByParentId(String parentId) {
		return siteMapper.getMenuListByParentId(parentId);
	}

	@Override
	public List<SiteCategory> getSiteCategoryListBySiteId(String siteId) {
		return siteMapper.getSiteCategoryListBySiteId(siteId);
	}

	@Override
	public List<Menu> getChildrenMenuList() {
		return siteMapper.getChildrenMenuList();
	}
	
}
