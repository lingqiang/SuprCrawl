package com.cmge.ad.service;

import java.util.HashMap;
import java.util.List;

import com.cmge.ad.model.Menu;
import com.cmge.ad.model.Site;
import com.cmge.ad.model.SiteCategory;
import com.cmge.ad.util.Pager;


/**
 * @desc	站点业务类
 * @author	ljt
 * @time	2014-10-15 下午5:58:37
 */
public interface SiteService {

	List<Site> getAllSiteList(int type);

	List<SiteCategory> getCategoryBySiteId(String siteId);

	void getSiteList(Pager<Site> pager, HashMap<String, Object> paramMap);

	void getSiteCategoryList(Pager<SiteCategory> pager, HashMap<String, Object> paramMap);

	void getMenuList(Pager<Menu> pager, HashMap<String, Object> paramMap);

	List<Menu> getParentMenu();

	void addChildMenu(Menu menu);

	boolean existMenu(Menu menu);

	void deleteChildMenuById(Integer id);

	boolean existMenuRelation(Integer id);

	/**获取所有站点信息**/
	List<Site> getAllSiteInfoList();

	/**通过父类id获取子类栏目**/
	List<Menu> getMenuListByParentId(String parentId);

	/**根据站点id选择分类**/
	List<SiteCategory> getSiteCategoryListBySiteId(String siteId);

	/**获取所有子类栏目**/
	List<Menu> getChildrenMenuList();

}
