package com.cmge.ad.service;

import com.cmge.ad.model.BindRelation;
import com.cmge.ad.util.Pager;

/**
 * @desc 绑定关系业务
 * @author liming
 * @time 2015-3-4
 */
public interface BindRelationService {

	/**获取绑定关系列表**/
	void getBindRelationList(Pager<BindRelation> pager,BindRelation bindRelation);

	/** 解除绑定关系**/
	int unbindRealtionById(Integer id);

	/**保存绑定关系**/
	void saveBindRelation(BindRelation bindRelation);

	/**是否已存在绑定关系**/
	Boolean isExistBindRelation(BindRelation bindRelation);
}
