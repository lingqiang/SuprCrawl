package com.cmge.ad.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.cmge.ad.mapper.BindRelationMapper;
import com.cmge.ad.model.BindRelation;
import com.cmge.ad.service.BindRelationService;
import com.cmge.ad.util.Pager;

@Service
@Transactional(isolation=Isolation.DEFAULT,rollbackFor=Exception.class)
public class BindRelationServiceImpl implements BindRelationService{
	
	public static final Logger logger = Logger.getLogger(BindRelationServiceImpl.class);
	
	@Autowired
	private BindRelationMapper bindRelationMapper;

	@Override
	public void getBindRelationList(Pager<BindRelation> pager,BindRelation bindRelation) {
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", pager.getStart());
		paramMap.put("limit", pager.getLimit());
		paramMap.put("menuId", bindRelation.getMenuId());
		paramMap.put("siteId", bindRelation.getSiteId());
		paramMap.put("siteCategoryId", bindRelation.getSiteCategoryId());
		int count = bindRelationMapper.getBindRelationCount(paramMap);
		pager.setTotalCount(count);
		if(count > 0){
			List<BindRelation> b = bindRelationMapper.getBindRelationList(paramMap);
			pager.setRows(b);
		}
	}

	@Override
	public int unbindRealtionById(Integer id) {
		return bindRelationMapper.unbindRealtionById(id);
	}

	@Override
	public void saveBindRelation(BindRelation bindRelation) {
		bindRelationMapper.saveBindRelation(bindRelation);
	}

	@Override
	public Boolean isExistBindRelation(BindRelation bindRelation) {
		return bindRelationMapper.isExistBindRelation(bindRelation)>0 ? true : false;
	}
	
	
}
