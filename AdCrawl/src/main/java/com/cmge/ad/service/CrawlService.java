package com.cmge.ad.service;

import java.util.HashMap;

import com.cmge.ad.model.AlbumStore;
import com.cmge.ad.model.ArticleStore;
import com.cmge.ad.model.Crawl;
import com.cmge.ad.model.LockPaint;
import com.cmge.ad.model.PaintReq;
import com.cmge.ad.model.PaintResp;
import com.cmge.ad.model.PictureStore;
import com.cmge.ad.util.Pager;


/**
 * @desc	爬虫业务类
 * @author	ljt
 * @time	2014-10-15 下午5:58:37
 */
public interface CrawlService {

	void addCrawl(Crawl crawl);

	void runArticleCrawlTask(Crawl crawl);
	
	void runAlbumCrawlTask(Crawl crawl);

	Crawl scanCrawlTask();

	void addArticleStore(ArticleStore articleStore);

	boolean isExistArticleUniqueId(String uniqueId);

	void starCralTask(Crawl crawl);

	void getCrawlList(Pager<Crawl> pager, HashMap<String, Object> paramMap);

	Crawl scanCrawlTaskById(String id);

	void starCralTaskById(String id);

	void updateCrawlInfo(Crawl crawl);

	boolean isExistAlbumUniqueId(String uniqueId);

	void addAlbumStore(AlbumStore albumStore);

	void addPictureStore(PictureStore ps);

	void addAlbumCrawlInfo(Crawl crawl);

	void runCrawlTask(Crawl crawl);

	void autoCrawlArticle();

	void savePaint(LockPaint paint);

	PaintResp getPaintList(PaintReq getReq);

}
