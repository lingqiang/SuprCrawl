package com.cmge.ad.spider;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @desc	爬虫任务管理器
 * @author	ljt
 * @time	2015-2-1 下午8:27:28
 */
public class PorcessorManager {
	
	private static ConcurrentHashMap<String,ArticleListPageProcessor> articleProcessorMap = new ConcurrentHashMap<String,ArticleListPageProcessor>();
	
	private static ConcurrentHashMap<String,AlbumListPageProcessor> albumProcessorMap = new ConcurrentHashMap<String,AlbumListPageProcessor>();
	
	public static void addArticleProcessor(ArticleListPageProcessor processor){
		articleProcessorMap.put(processor.getId(),processor);
	}
	
	public static void addAlbumProcessor(AlbumListPageProcessor processor){
		albumProcessorMap.put(processor.getId(),processor);
	}
	
	public static void removeArticleProcessor(String id){
		articleProcessorMap.remove(id);
	}
	
	public static void removeAlbumProcessor(String id){
		albumProcessorMap.remove(id);
	}
	
	public static boolean stopArticleProcessorById(String id){
		ArticleListPageProcessor processor = articleProcessorMap.get(id);
		if(null != processor){
			processor.stopCrawl();
			removeArticleProcessor(id);
		}
		
		return true;
	}
	
	public static boolean stopAlbumProcessorById(String id){
		AlbumListPageProcessor processor = albumProcessorMap.get(id);
		if(null != processor){
			processor.stopCrawl();
			removeAlbumProcessor(id);
		}
		
		return true;
	}
	
}
