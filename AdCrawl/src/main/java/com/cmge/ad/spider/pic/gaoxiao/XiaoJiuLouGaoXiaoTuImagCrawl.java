package com.cmge.ad.spider.pic.gaoxiao;

import java.util.List;

import org.springframework.util.StringUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import com.cmge.ad.model.Picture;
import com.cmge.ad.spider.pipeline.MysqlPicturePipeline;

/**
 * @desc	笑酒楼内涵图 图片抓取   已爬
 * 			不要开启多线程  该网站有安全狗会拦截请求
 * 			http://www.xiaojiulou.com/gaoxiaotupian/
 * 
 * @author	ljt
 * @time	2014-12-30 下午7:51:34
 */
public class XiaoJiuLouGaoXiaoTuImagCrawl implements PageProcessor {
	
    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    public static final String URL_LIST = "list_\\w+";
    
    // 列表最大值
    private int max = 49;
    
    @Override
    public void process(Page page) {
    	if (page.getUrl().regex(URL_LIST).match()) {
    		// 检索当前页面所有段子
        	List<Selectable> cList = page.getHtml().xpath("//div[@class='paihang']/ul//li").nodes();
    		if(null != cList && cList.size() > 0){
    			for(Selectable str : cList){
    				String url = str.xpath("//a[@target='_blank']/@href").get().toString();
    				if(!StringUtils.isEmpty(url)){
    					page.addTargetRequest(url);
    				}
    			}
    		}
    		
    		// 当前页
    		String url = page.getUrl().get();
    		int current = 1;
    		try {
    			current = Integer.parseInt(url.substring(url.lastIndexOf("_")+1,url.lastIndexOf(".html")));
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    		
    		System.out.println("current is "+current);
    		if(current < max){
    			page.addTargetRequest("http://www.xiaojiulou.com/gaoxiaotupian/list_"+(current+1)+".html");
    		}
        } else {
        	Picture pic = new Picture();
        	String url = page.getHtml().xpath("//div[@class='zw']//img[@class='maximg']/@src").get().toString();
        	String desc = page.getHtml().xpath("//div[@class='zw']//img[@class='maximg']/@alt").get().toString();
        	if(!StringUtils.isEmpty(url)){
        		pic.setDesc(desc);
        		pic.setMinImageUrl(url);
        		pic.setMaxImageUrl(url);
        		pic.setSource("xiaojiulou_gaoxiaotu");
        		page.putField("picture",pic);
        	}
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new XiaoJiuLouGaoXiaoTuImagCrawl())
    					.addUrl("http://www.xiaojiulou.com/gaoxiaotupian/list_1.html")
//    					.addPipeline(new RedisPipeline())
//    					.addPipeline(new JsonFilePipeline())
//    					.addPipeline(new JsonPipeline())
    					.addPipeline(new MysqlPicturePipeline())
    					.thread(1);
    	qsSpider.start();
    }	
	
	
}
