package com.cmge.ad.spider.article.qiushi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cmge.ad.cache.redis.RedisCache;
import com.cmge.ad.model.Article;
import com.cmge.ad.util.JsonUtil;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 * @desc	存放redis实现
 * @author	ljt
 * @time	2014-12-29 下午7:22:48
 */
@Component
public class RedisPipeline implements Pipeline{
	
//	@Autowired
	private RedisCache redisCache;
	
	@Override
	public void process(ResultItems resultItems, Task task) {
		Article article = (Article)resultItems.get("article");
		// 添加到redis中
		redisCache.add("articleList", JsonUtil.toJson(article));
	}
	
}
