package com.cmge.ad.spider.pic.gaoxiao;

import java.util.List;

import org.springframework.util.StringUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import com.cmge.ad.model.Picture;
import com.cmge.ad.spider.pipeline.MysqlPicturePipeline;

/**
 * @desc 	搞笑吧爆笑图 图片抓取 已爬 
 * 			
 * 			该站开启了防盗链
 * 
 * 			http://www.hugao8.com/category/pic/baoxiaotupian/page/2/
 * 
 * @author 	ljt
 * @time 	2014-12-30 下午7:51:34
 */
public class GaoXiaoBaBaoXiaoTuImagCrawl implements PageProcessor {

	private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

	public static final String URL_LIST = "/baoxiaotupian/page/\\w+";

	// 列表最大值
	private int max = 66;

	@Override
	public void process(Page page) {
		if (page.getUrl().regex(URL_LIST).match()) {
			// 检索当前页面所有段子
			List<Selectable> cList = page.getHtml().xpath("//div[@class='bd']//ul[@class='clearfix pList']//li").nodes();
			if (null != cList && cList.size() > 0) {
				for (Selectable str : cList) {
					String url = str.xpath("//div[@class='picBox']//a[@class='pic']/@href").get().toString();
					if (!StringUtils.isEmpty(url)) {
						page.addTargetRequest(url);
					}
				}
			}

			// 当前页
			String url = page.getUrl().get();
			int current = 1;
			try {
				current = Integer.parseInt(url.substring(url.lastIndexOf("/") + 1, url.length()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println("current is " + current);
			if (current < max) {
				page.addTargetRequest("http://www.hugao8.com/category/pic/baoxiaotupian/page/" + (current + 1));
			}
		} else {
			Picture pic = new Picture();
			String desc = page.getHtml().xpath("//div[@class='postmeta']/h1/text()").get().toString();
			String url = page.getHtml().xpath("//div[@id='infoMain']//img/@src").get().toString();
			if (!StringUtils.isEmpty(url)) {
				pic.setDesc(desc);
				pic.setMinImageUrl(url);
				pic.setMaxImageUrl(url);
				pic.setSource("gaoxiaoba_gaoxiaotu");
				page.putField("picture", pic);
			}
		}
	}

	@Override
	public Site getSite() {
		return site;
	}

	public static void main(String[] args) throws Exception {
		Spider qsSpider = Spider.create(new GaoXiaoBaBaoXiaoTuImagCrawl())
				.addUrl("http://www.hugao8.com/category/pic/baoxiaotupian/page/2")
			// .addPipeline(new RedisPipeline())
			// .addPipeline(new JsonFilePipeline())
			// .addPipeline(new JsonPipeline())
				.addPipeline(new MysqlPicturePipeline())
				.thread(1);
		qsSpider.start();
	}

}
