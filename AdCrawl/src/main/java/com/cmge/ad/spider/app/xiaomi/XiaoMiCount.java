package com.cmge.ad.spider.app.xiaomi;

import java.util.List;

public class XiaoMiCount {
	
	private List<XiaoMi> data;
	
	private int count;

	public List<XiaoMi> getData() {
		return data;
	}

	public void setData(List<XiaoMi> data) {
		this.data = data;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
