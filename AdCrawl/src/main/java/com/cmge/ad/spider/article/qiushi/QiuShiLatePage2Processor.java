package com.cmge.ad.spider.article.qiushi;

import java.util.List;

import org.springframework.util.StringUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import com.cmge.ad.model.Article;

/**
 * @desc	糗事百科最新
 * 
 * 		1、抓取列表
 * 		2、列表中抓取具体文章
 * 		3、具体文章中抓取指定内容
 * 
 * @author	ljt
 * @time	2014-12-29 上午11:16:05
 */
public class QiuShiLatePage2Processor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    public static final String URL_LIST = "/late/page/\\w+";

    public static final String URL_POST = "/article/\\w+";
    
    @Override
    public void process(Page page) {
        if (page.getUrl().regex(URL_LIST).match()) {
        	// 列表页
        	List<String> listUrl = page.getHtml().xpath("//div[@class='article block untagged mb15']//ul[@class='clearfix']//li[@class='comments']/a/@href").links().regex(URL_LIST).all();
        	if(null == listUrl || listUrl.size() == 0){
        		// 截取url的末尾数据拼装
        		String url = page.getUrl().get();
        		int pageNo;
				try {
					pageNo = Integer.parseInt(url.substring(url.lastIndexOf("/")+1,url.length()));
					listUrl.add(url.replace("/late/page/"+pageNo, "/late/page/"+(pageNo+1)));
				} catch (Exception e) {
					e.printStackTrace();
				}
        	}
        	
        	page.addTargetRequests(listUrl);
            page.addTargetRequests(page.getHtml().xpath("//div[@class=\"qiushi\"]").links().regex(URL_POST).all());
        } else {
        	Article article = new Article();
        	// 内容
        	String content = page.getHtml().xpath("//div[@class='qiushi']/text(0)").get();
        	if(StringUtils.isEmpty(content)){
        		// 段子内容为空 则跳过
        		page.setSkip(true);
        	}
        	// 小图片
        	String minPicUrl = page.getHtml().xpath("//div[@class='qiushi']/a/img/@src").get();
        	if(!(null != minPicUrl && minPicUrl.indexOf(".jpg")==minPicUrl.length()-4 && minPicUrl.contains("small"))){
        		minPicUrl = "";
        	}
        	// 大图片
        	String maxPicUrl = page.getHtml().xpath("//div[@class='qiushi']/a/@href").get();
        	if(!(null != maxPicUrl && minPicUrl.indexOf(".jpg")==minPicUrl.length()-4 && maxPicUrl.contains("medium"))){
        		maxPicUrl = "";
        	}
        	// 顶
        	String zan = page.getHtml().xpath("//p[@class='vote']/a[1]/text()").get();
        	
        	article.setContent(content.trim());
        	article.setMinImageUrl(minPicUrl);
        	article.setMaxImageUrl(maxPicUrl);
        	article.setZan(Integer.parseInt(zan == null ? "0" : zan));
        	
        	page.putField("article",article);
        	// 作者
//        	String author = page.getHtml().xpath("//p[@class='user']/a/text(0)").get();
//        	// 踩
//        	String cai = page.getHtml().xpath("//p[@class='vote']/a[2]/text()").get();
//        	// 评论
//        	String commentCount = page.getHtml().xpath("//p[@class='vote']/a[3]/strong/text()").get();
//        	
//        	List<Comment> commentList = new ArrayList<Comment>();
//        	// 评论大于0
//        	if(null != commentCount && Integer.parseInt(commentCount) > 0){
//        		List<Selectable> cList = page.getHtml().xpath("//div[@class='comments-list']//div[@class='qiushi']").nodes();
//        		if(null != cList && cList.size() > 0){
//        			for(Selectable str : cList){
//        				Comment comment = new Comment();
//            			String nickName = str.xpath("//a/text()").get();
//            			String cmCotent = str.xpath("//span/text()").get();
//            			comment.setContent(cmCotent);
//            			comment.setUserName(nickName);
//            			commentList.add(comment);
//            		}
//        		}
//        	}
        	
//        	article.setAuthor(author);
//        	article.setCai(Integer.parseInt(cai == null ? "0" : cai));
//        	article.setCommentCount(commentList.size());
//        	article.setCommentList(commentList);
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new QiuShiLatePage2Processor())
    					.addUrl("http://wap3.qiushibaike.com/late/page/1")
//    					.addPipeline(new RedisPipeline())
//    					.addPipeline(new JsonFilePipeline())
    					.addPipeline(new JsonPipeline())
    					.thread(1);
    	qsSpider.start();
    }
}
