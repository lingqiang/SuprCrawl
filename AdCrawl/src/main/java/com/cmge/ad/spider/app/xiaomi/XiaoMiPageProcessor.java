package com.cmge.ad.spider.app.xiaomi;

import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import com.cmge.ad.model.App;
import com.cmge.ad.util.JsonUtil;

/**
 * @desc	小米app抓取
 * 
 * 			http://app.mi.com/categotyAllListApi?page=0&categoryId=16&pageSize=256
 * 
 * @author	ljt
 * @time	2015-2-7 下午3:49:38
 */
public class XiaoMiPageProcessor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    public static final String URL_LIST = "\\w+#page=\\w+";

    public static final String URL_POST = "\\w+detail/\\w+";
    
    private boolean listFlag = true;
    
    @Override
    public void process(Page page) {
        if (page.getUrl().regex(URL_LIST).match()) {
        	// 列表页
        	List<String> listUrl = page.getHtml().xpath("//div[@class='applist-wrap']/ul/li/a/@href").all();
        	page.addTargetRequests(listUrl);
        	if(listFlag){
//        		List<String> urlList = new ArrayList<String>();
//        		for(int i = 1;i<=8;i++){
//        			String url = "http://app.mi.com/category/16#page="+i;
//        			urlList.add(url);
//        		}
//        		page.addTargetRequests(urlList);
        		listFlag = false;
        	}
        } else {
        	App app = new App();
        	
        	String company = page.getHtml().xpath("//div[@class='intro-titles']/p/text()").get();
        	String appName = page.getHtml().xpath("//div[@class='intro-titles']/h3/text()").get();
        	String iconUrl = page.getHtml().xpath("//div[@class='app-info']/img[@class='yellow-flower']/@src").get();
        	String downloadUrl = page.getHtml().xpath("//div[@class='app-info-down']/a/@href").get();
        	String appSize = page.getHtml().xpath("//div[@class='details preventDefault']/ul/li[2]/text()").get();
        	String appVersion = page.getHtml().xpath("//div[@class='details preventDefault']/ul/li[4]/text()").get();
        	String updateTime = page.getHtml().xpath("//div[@class='details preventDefault']/ul/li[6]/text()").get();
        	String packageName = page.getHtml().xpath("//div[@class='details preventDefault']/ul/li[8]/text()").get();
        	String description = page.getHtml().xpath("//div[@class='app-text']/p[@class='pslide']/text()").get();
        	
        	List<String> picList = page.getHtml().xpath("//div[@class='img-list height-auto']/img/@src").all();
        	List<String> authList = page.getHtml().xpath("//ul[@class='second-ul']/li/text()").all();
        	
        	app.setRemoteUrl(page.getUrl().toString());
        	app.setCompany(company);
        	app.setAppName(appName);
        	app.setIconUrl(iconUrl);
        	app.setDownloadUrl(downloadUrl);
        	app.setAppSize(appSize);
        	app.setAppVersion(appVersion);
        	app.setUpdateTime(updateTime);
        	app.setPackageName(packageName);
        	app.setDescription(description);
        	app.setPicList(picList);
        	app.setAuthList(authList);
        	app.setUniqueId(page.getUrl().toString());
        	
        	System.out.println(JsonUtil.toJson(app));
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new XiaoMiPageProcessor())
    					.addUrl("http://app.mi.com/category/16#page=5")
    					.addUrl("http://app.mi.com/category/16#page=6")
    					.addUrl("http://app.mi.com/category/16#page=7")
    					.addUrl("http://app.mi.com/category/16#page=8")
    					.thread(1);
    	qsSpider.start();
    }
}
