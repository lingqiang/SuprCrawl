package com.cmge.ad.spider.app.flyme;

import java.util.ArrayList;
import java.util.List;

import net.sf.ehcache.search.aggregator.Max;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import com.cmge.ad.model.App;
import com.cmge.ad.util.HttpClientUtil;
import com.cmge.ad.util.JsonUtil;
import com.cmge.ad.util.SuprUtil;

/**
 * @desc	魅族app抓取
 * 
 * 			http://app.meizu.com/games/public/category/9012/free/feed/0/1800
 * 
 * @author	ljt
 * @time	2015-2-7 下午3:49:38
 */
public class FlymeJsonProcessor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    private boolean listFlag = true;
    
    private static List<String> list;
    
    static{
    	list = new ArrayList<String>();
    	list.add("1006");
    	list.add("1000");
    	list.add("1005");
    	list.add("1001");
    	list.add("1004");
    	list.add("9012");
    	list.add("1003");
    	list.add("1007");
    	list.add("9013");
    	list.add("1002");
    }
    
    @Override
    public void process(Page page) {
    	if(listFlag){
    		int i = 0;
    		for(String index : list){
    			String respStr = HttpClientUtil.get("http://app.meizu.com/games/public/category/"+index+"/free/feed/0/"+Integer.MAX_VALUE);
    			FlymeCount count = JsonUtil.getGson().fromJson(respStr, FlymeCount.class);
    			List<Flyme> flymeList = count.getValue();
    			if(!SuprUtil.isEmptyCollection(flymeList)){
    				for(Flyme flyme : flymeList){
    					page.addTargetRequest("http://app.meizu.com/games/public/detail?package_name="+flyme.getPackageName());
    					i++;
    				}
    			}
    		}
    		System.out.println("++++++++++++++++++++++++++++"+i+"++++++++++++++++++++++++++++");
    		listFlag = false;
    	}else{
    		App app = new App();
        	
        	String company = page.getHtml().xpath("//div[@class='app_content ellipsis']/a[@class='current']/@title").get();
        	String appName = page.getHtml().xpath("//div[@class='detail_top']/h3/text()").get();
        	String iconUrl = page.getHtml().xpath("//img[@class='app_img']/@src").get();
        	
        	// http://app.meizu.com/games/public/download.json?app_id=2063546
        	String downloadUrl = page.getHtml().xpath("//div[@class='price_bg downloading']/@data-appid").get();
        	
        	String appSize = page.getHtml().xpath("//div[@class='app_download download_container']/ul/li[6]/div/text()").get();
        	String appVersion = page.getHtml().xpath("//div[@class='app_download download_container']/ul/li[4]/div/text()").get();
        	String updateTime = page.getHtml().xpath("//div[@class='app_download download_container']/ul/li[7]/div/text()").get();
        	String packageName = page.getUrl().toString().split("=")[1];
        	String description = page.getHtml().xpath("//div[@class='description_detail']/text()").get();
        	
        	List<String> picList = page.getHtml().xpath("//img[@class='screenImg1']/@src").all();
//        	List<String> authList = page.getHtml().xpath("//ul[@class='second-ul']/li/text()").all();
        	
        	app.setRemoteUrl(page.getUrl().toString());
        	app.setCompany(company);
        	app.setAppName(appName);
        	app.setIconUrl(iconUrl);
        	app.setDownloadUrl(downloadUrl);
        	app.setAppSize(appSize);
        	app.setAppVersion(appVersion);
        	app.setUpdateTime(updateTime);
        	app.setPackageName(packageName);
        	app.setDescription(description);
        	app.setPicList(picList);
//        	app.setAuthList(authList);
        	app.setUniqueId(page.getUrl().toString());
        	
        	System.out.println(JsonUtil.toJson(app));
    	}
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new FlymeJsonProcessor())
    					.addUrl("http://www.baidu.com")
    					.thread(5);
    	qsSpider.start();
    }
}
