package com.cmge.ad.spider.app.flyme;

public class Flyme {
	
	private String packageName;

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
}
