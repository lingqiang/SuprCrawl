package com.cmge.ad.spider.app.yingyongbao;

import java.util.ArrayList;
import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import com.cmge.ad.model.App;
import com.cmge.ad.spider.app.xiaomi.XiaoMi;
import com.cmge.ad.spider.app.xiaomi.XiaoMiCount;
import com.cmge.ad.util.HttpClientUtil;
import com.cmge.ad.util.JsonUtil;
import com.cmge.ad.util.SuprUtil;
import com.google.gson.JsonSyntaxException;

/**
 * @desc	应用宝app抓取
 * 
 * 			http://sj.qq.com/myapp/cate/appList.htm?orgame=2&pageSize=200000&categoryId=149
 * 
 * @author	ljt
 * @time	2015-2-7 下午3:49:38
 */
public class QQJsonProcessor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    private boolean listFlag = true;
    
    private static List<String> list;
    
    static{
    	list = new ArrayList<String>();
    	list.add("121");
    	list.add("144");
    	list.add("146");
    	list.add("147");
    	list.add("148");
    	list.add("149");
    	list.add("151");
    	list.add("153");
    }
    
    @Override
    public void process(Page page) {
    	if(listFlag){
    		int i = 0;
    		for(String index : list){
    			String respStr = HttpClientUtil.get("http://sj.qq.com/myapp/cate/appList.htm?orgame=2&categoryId="+index+"&pageSize="+Integer.MAX_VALUE);
    			QQCount count;
				try {
					count = JsonUtil.getGson().fromJson(respStr, QQCount.class);
				} catch (JsonSyntaxException e) {
					continue;
				}
    			List<QQ> qqList = count.getObj();
    			if(!SuprUtil.isEmptyCollection(qqList)){
    				for(QQ qq : qqList){
    					page.addTargetRequest("http://sj.qq.com/myapp/detail.htm?apkName="+qq.getPkgName());
    					i++;
    				}
    			}
    		}
    		System.out.println("++++++++++++++++++++++++++++"+i+"++++++++++++++++++++++++++++");
    		listFlag = false;
    	}else{
    		App app = new App();
        	
        	String company = page.getHtml().xpath("//div[@class='det-othinfo-container J_Mod']/div[6]/text()").get();
        	String appName = page.getHtml().xpath("//div[@class='det-name-int']/text()").get();
        	String iconUrl = page.getHtml().xpath("//div[@class='det-icon']/img/@src").get();
        	String downloadUrl = page.getHtml().xpath("//div[@class='det-ins-btn-box']/a[@class='det-ins-btn']/@ex_url").get();
        	String appSize = page.getHtml().xpath("//div[@class='det-size']/text()").get();
        	String appVersion = page.getHtml().xpath("//div[@class='det-othinfo-data']/text()").get();
        	String updateTime = page.getHtml().xpath("//div[@class='det-othinfo-container J_Mod']/div[4]/text()").get();
        	String packageName = page.getUrl().toString().split("=")[1];
        	String description = page.getHtml().xpath("//div[@class='det-app-data-info']/text()").get();
        	
        	List<String> picList = page.getHtml().xpath("//div[@class='det-pic-scroll-container']//img/@data-src").all();
        	
        	app.setRemoteUrl(page.getUrl().toString());
        	app.setCompany(company);
        	app.setAppName(appName);
        	app.setIconUrl(iconUrl);
        	app.setDownloadUrl(downloadUrl);
        	app.setAppSize(appSize);
        	app.setAppVersion(appVersion);
        	app.setUpdateTime(updateTime);
        	app.setPackageName(packageName);
        	app.setDescription(description);
        	app.setPicList(picList);
        	app.setUniqueId(page.getUrl().toString());
        	
        	System.out.println(JsonUtil.toJson(app));
    	}
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new QQJsonProcessor())
    					.addUrl("http://www.baidu.com")
    					.thread(5);
    	qsSpider.start();
    }
}
