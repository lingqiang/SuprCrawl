package com.cmge.ad.interfaces.vo;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented  
@Retention(RetentionPolicy.RUNTIME)  
@Target(ElementType.FIELD)  
public @interface ParamValidate {
	
	//是否为必填
	public boolean require();
	
	//校验的正则
	public String regex();
}
