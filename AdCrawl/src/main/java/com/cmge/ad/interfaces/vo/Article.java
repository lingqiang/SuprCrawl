package com.cmge.ad.interfaces.vo;


/**
 * @desc	段子
 * @author	ljt
 * @time	2014-12-29 下午6:55:22
 */
public class Article {
	
	private Integer id;
	
	// 段子内容
	private String content;
	
	// 大图
	private String maxImageUrl;
	
	// 小图
	private String minImageUrl;
	
	private String locationUrl;
	
	// 1：无图  2：有图
	private int type;
	
	// 赞
	private int zan;

	private String key;
	
	private String answer;
	
	private int width;
	
	private int height;
	
	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMaxImageUrl() {
		return maxImageUrl;
	}

	public void setMaxImageUrl(String maxImageUrl) {
		this.maxImageUrl = maxImageUrl;
	}

	public String getMinImageUrl() {
		return minImageUrl;
	}

	public void setMinImageUrl(String minImageUrl) {
		this.minImageUrl = minImageUrl;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getZan() {
		return zan;
	}

	public void setZan(int zan) {
		this.zan = zan;
	}

	@Override
	public String toString() {
		return "Article [content=" + content + ", maxImageUrl=" + maxImageUrl + ", minImageUrl=" + minImageUrl + ", type=" + type + ",  key=" + key + "]";
	}
}
