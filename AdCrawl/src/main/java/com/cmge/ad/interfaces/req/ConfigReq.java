package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;
import com.cmge.ad.interfaces.vo.ParamValidate;

/**
 * @desc	系统配置项请求对象
 * @author	ljt
 * @time	2015-2-11 下午2:13:27
 */
public class ConfigReq extends BaseReqMsg {
	
	@ParamValidate(require=true,regex="^[\\S]{1,22}$")
	private String configKey;
	
	public ConfigReq() {
	}
	
	public ConfigReq(String configKey) {
		super();
		this.configKey = configKey;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}
}
