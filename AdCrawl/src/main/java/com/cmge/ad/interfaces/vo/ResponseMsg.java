package com.cmge.ad.interfaces.vo;

import com.cmge.ad.util.Constant;
import com.cmge.ad.util.config.CompositeFactory;

public class ResponseMsg extends BaseMsg {
	
	/** 0：成功 */
	private int resultCode;
	
	/** 失败信息 */
	private String errMsg;

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
		// 获取错误信息
		if(resultCode != Constant.DEAL_SUCCESS){
			this.errMsg = CompositeFactory.getInstance().getString(String.valueOf(resultCode));
		}
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
}
