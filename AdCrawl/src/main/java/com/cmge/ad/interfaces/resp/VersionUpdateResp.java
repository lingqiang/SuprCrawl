package com.cmge.ad.interfaces.resp;

import com.cmge.ad.interfaces.vo.ResponseMsg;
import com.cmge.ad.interfaces.vo.Version;

public class VersionUpdateResp extends ResponseMsg {

	private Version version;
	
	public Version getVersion() {
		return version;
	}

	public void setVersion(Version version) {
		this.version = version;
	}

	public VersionUpdateResp() {
	}
	
	public VersionUpdateResp(int resultCode) {
		setResultCode(resultCode);
	}

}
