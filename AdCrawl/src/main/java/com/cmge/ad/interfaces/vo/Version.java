package com.cmge.ad.interfaces.vo;

public class Version {
	
	private int isNeedUpdate;
	
	private String url;
	
	private int versionCode;
	
	private String updateDesc;
	
	private String versionName;

	public int getIsNeedUpdate() {
		return isNeedUpdate;
	}

	public void setIsNeedUpdate(int isNeedUpdate) {
		this.isNeedUpdate = isNeedUpdate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

	public String getUpdateDesc() {
		return updateDesc;
	}

	public void setUpdateDesc(String updateDesc) {
		this.updateDesc = updateDesc;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
}
