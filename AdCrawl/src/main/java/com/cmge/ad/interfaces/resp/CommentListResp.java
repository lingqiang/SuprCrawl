package com.cmge.ad.interfaces.resp;

import java.util.List;

import com.cmge.ad.interfaces.vo.ResponseMsg;
import com.cmge.ad.model.Comment;

public class CommentListResp extends ResponseMsg {

	private List<Comment> commentList;
	
	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public CommentListResp() {
	}
	
	public CommentListResp(int resultCode) {
		setResultCode(resultCode);
	}

}
