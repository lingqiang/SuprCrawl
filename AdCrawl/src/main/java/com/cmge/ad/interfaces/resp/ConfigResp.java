package com.cmge.ad.interfaces.resp;

import com.cmge.ad.interfaces.vo.ResponseMsg;

public class ConfigResp extends ResponseMsg {

	// 配置项值
	private String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ConfigResp() {
	}
	
	public ConfigResp(int resultCode) {
		setResultCode(resultCode);
	}

}
