package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;
import com.cmge.ad.interfaces.vo.ParamValidate;

public class PictureListReq extends BaseReqMsg {
	
	@ParamValidate(require=true,regex="^[\\S]{1,22}$")
	private String menuCode;
	
	@ParamValidate(require = true, regex = "^[\\d]{1,2}$")
	private int pageSize;
	
	// 刷新方向  1：最新 2：老数据
	@ParamValidate(require=true,regex="^[1|2]$")
	private int orientation;
	
	@ParamValidate(require = true, regex = "^[\\d]{1,11}$")
	private Integer lastId;

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getOrientation() {
		return orientation;
	}

	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	public int getLastId() {
		return lastId;
	}

	public void setLastId(int lastId) {
		this.lastId = lastId;
	}
}
