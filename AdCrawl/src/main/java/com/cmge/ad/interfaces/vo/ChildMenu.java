package com.cmge.ad.interfaces.vo;

import java.io.Serializable;

public class ChildMenu implements Serializable{
	
	private String menuName;
	
	private String menuCode;

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
}
