package com.cmge.ad.mapper;

import java.util.List;
import java.util.Map;

import com.cmge.ad.model.BindRelation;

public interface BindRelationMapper {

	/**获取绑定关系总数**/
	int getBindRelationCount(Map<String,Object> paramMap);

	/**获取绑定关系列表**/
	List<BindRelation> getBindRelationList(Map<String,Object> paramMap);

	/**解除绑定关系**/
	int unbindRealtionById(Integer id);
	
	/**保存绑定关系**/
	void saveBindRelation(BindRelation bindRelation);
	
	/**是否已存在绑定关系**/
	int isExistBindRelation(BindRelation bindRelation);
}
