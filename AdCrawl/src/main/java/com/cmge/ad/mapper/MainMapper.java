package com.cmge.ad.mapper;

import java.util.List;

import com.cmge.ad.interfaces.req.AlbumListReq;
import com.cmge.ad.interfaces.req.ArticleListReq;
import com.cmge.ad.interfaces.req.CommentListReq;
import com.cmge.ad.interfaces.req.CommentReq;
import com.cmge.ad.interfaces.req.ConfigReq;
import com.cmge.ad.interfaces.req.FeedBackReq;
import com.cmge.ad.interfaces.req.PictureListReq;
import com.cmge.ad.interfaces.req.RegisterReq;
import com.cmge.ad.interfaces.req.VersionUpdateReq;
import com.cmge.ad.interfaces.req.ZanCommentReq;
import com.cmge.ad.interfaces.vo.Article;
import com.cmge.ad.interfaces.vo.ChildMenu;
import com.cmge.ad.interfaces.vo.ParentMenu;
import com.cmge.ad.interfaces.vo.Album;
import com.cmge.ad.interfaces.vo.Picture;
import com.cmge.ad.interfaces.vo.Version;
import com.cmge.ad.model.Comment;
import com.cmge.ad.model.User;


public interface MainMapper {

	void zanArticle(Integer id);

	void zanPicture(Integer id);

	void zanAlbum(Integer id);

	List<ParentMenu> getParentMenu();

	List<ChildMenu> getChildMenuByPId(Integer id);

	List<Article> getOldArticleList(ArticleListReq req);

	List<Article> getNewArticleList(ArticleListReq req);

	List<Album> getNewAlbumList(PictureListReq req);

	List<Album> getOldAlbumList(PictureListReq req);

	List<Picture> getPictureListByAlbumId(Integer id);

	void feedBack(FeedBackReq req);

	Version versionUpdate(VersionUpdateReq req);

	List<Article> getNewHotArticleList(ArticleListReq req);

	List<Article> getOldHotArticleList(ArticleListReq req);

	String getConfigValue(ConfigReq configReq);

	List<Album> getNewAlbum(AlbumListReq req);

	List<Album> getOldAlbum(AlbumListReq req);

	void register(RegisterReq registerReq);

	void comment(CommentReq commentReq);

	void zanComment(ZanCommentReq zanReq);

	List<Comment> getCommentList(CommentListReq commentReq);

	int exist(User user);

	void addUser(User user);
}
