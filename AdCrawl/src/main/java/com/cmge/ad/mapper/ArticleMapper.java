package com.cmge.ad.mapper;

import java.util.HashMap;
import java.util.List;

import com.cmge.ad.model.Article;
import com.cmge.ad.model.ArticleStore;
import com.cmge.ad.model.Menu;


public interface ArticleMapper {

	int getArticleStoreCount(HashMap<String, Object> paramMap);

	List<ArticleStore> getArticleStoreList(HashMap<String, Object> paramMap);

	ArticleStore getArticleStoreInfoById(String id);

	String getArticleStoreId(ArticleStore article);

	void deleteArticleStoreById(Integer id);

	void syncArticleStore(HashMap<String,Object> param);

	int existSyncArticle(Integer id);

	void syncArticleStoreStatue(Integer id);

	List<ArticleStore> getSyncArticleList(int size);

	void addArticle(ArticleStore article);

	void deleteSyncArticle(ArticleStore article);

	void updateArticleStoreStatue(ArticleStore article);

	List<Menu> getArticleChildMenu();

	int getArticleCount(HashMap<String, Object> paramMap);

	List<Article> getArticleList(HashMap<String, Object> paramMap);

	List<Menu> getAllArticleMenu();

	Article getArticleInfoById(String id);

	String getArticleId(Article article);

	int getArticleStoreSum();

	void saveArticle(Article article);

	int delArticle(Integer id);
	
	int existSyncArticles(String[] id);

	String[] getNeedSyncIds(String[] id);

	List<Menu> getArticleMenuList();

	List<ArticleStore> getMenuArticleStoreList(HashMap<String, Object> param);
}
