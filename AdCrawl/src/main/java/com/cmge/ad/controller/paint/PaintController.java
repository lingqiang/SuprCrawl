package com.cmge.ad.controller.paint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmge.ad.controller.BaseController;
import com.cmge.ad.model.LockPaint;
import com.cmge.ad.model.PaintReq;
import com.cmge.ad.model.PaintResp;
import com.cmge.ad.model.Result;
import com.cmge.ad.service.CrawlService;
import com.cmge.ad.util.Constant;
import com.cmge.ad.util.JsonUtil;
import com.cmge.ad.util.TimeUtils;

/**
 * @desc	管理
 * @author	ljt
 * @time	2015-1-27 下午4:40:18
 */
@Controller
public class PaintController extends BaseController{
	
	public static final Logger logger = Logger.getLogger(PaintController.class);
	
	@Autowired
	private CrawlService crawlService;
	
	/**
	 * 跳转新增名画页面
	 */
	@RequestMapping("/addPaint")
	public String addPaint(){
		return "/adCrawl/paint/add_paint";
	}
	
	/**
	 * 保存名画
	 */
	@RequestMapping("/savePaint")
	public @ResponseBody
	Result savePaint(LockPaint paint){
		try {
			crawlService.savePaint(paint);
			return new Result("success", Constant.DEAL_SUCCESS);
		} catch (Exception e) {
			return new Result("fail", Constant.DEAL_EXCEPTION);
		}
	}
	
	/**
	 * 查询图片
	 */
	@RequestMapping("/getPaintList")
	public void getPaintList(PaintReq getReq,HttpServletRequest request,HttpServletResponse response){
		TimeUtils.mark();
		PaintResp getResp = null;
		try {
			if(null != getReq){
				// 业务处理
				getResp = crawlService.getPaintList(getReq);
			}
		} catch (Exception e) {
			logger.error("查询图片接口异常..",e);
			getResp = new PaintResp(Constant.DEAL_FAIL);
		} finally{
			String respStr = JsonUtil.toJson(getResp);
			JsonUtil.writeString(respStr,response);
			logger.info("查询图片接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
}
