package com.cmge.ad.controller.relation;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmge.ad.controller.BaseController;
import com.cmge.ad.model.BindRelation;
import com.cmge.ad.model.JsonResult;
import com.cmge.ad.model.Menu;
import com.cmge.ad.model.Result;
import com.cmge.ad.model.Site;
import com.cmge.ad.model.SiteCategory;
import com.cmge.ad.service.BindRelationService;
import com.cmge.ad.service.SiteService;
import com.cmge.ad.util.Constant;
import com.cmge.ad.util.Pager;
import com.cmge.ad.util.SuprUtil;

/**
 * @desc 	绑定关系
 * @author 	liming
 * @time 	2015-3-4
 */
@Controller
public class BindRelationController extends BaseController{
	
	public static final Logger logger = Logger.getLogger(BindRelationController.class);
	
	@Autowired
	private BindRelationService bindRelationService;
	
	@Autowired
	private SiteService siteService;
	
	/**
	 * 跳转绑定关系列表
	 */
	@RequestMapping("/bindRelationList")
	public String articleStoreList(){
		return "/adCrawl/relation/bing_relation_list";
	}
	
	/**
	 * 绑定关系列表
	 */
	@RequestMapping("/getBindRelationList")
	public @ResponseBody
	JsonResult getBindRelationList(Pager<BindRelation> pager,BindRelation bindRelation) {
		JsonResult result = new JsonResult();
		bindRelationService.getBindRelationList(pager,bindRelation);
		result.setData(pager.getRows());
		result.setTotalCount(pager.getTotalCount());
		return result;
	}
	
	/**
	 * 跳转绑定关系详情页面
	 */
	@RequestMapping("/loadBindRelationInfo")
	public String loadBindRelationInfo(ModelMap map) {
		//父类栏目
		List<Menu> menuList = siteService.getParentMenu();
		//所有站点数据
		List<Site> siteList = siteService.getAllSiteInfoList();
		
		map.addAttribute("menuList", menuList);
		map.addAttribute("siteList", siteList);
		return "/adCrawl/relation/add_bind_relation";
	}
	
	/***
	 * 根据父类栏目id选择栏目
	 * @param parentId 父类栏目id
	 * @return
	 */
	@RequestMapping("/getMenuListById")
	public @ResponseBody
	JsonResult getMenuListById(String parentId){
		JsonResult result = new JsonResult();
		List<Menu> menuList = siteService.getMenuListByParentId(parentId);
		Menu m = new Menu();
		m.setId(0);
		m.setMenuName("请选择");
		menuList.add(0,m);
		if(!SuprUtil.isEmptyCollection(menuList)){
			StringBuilder sb = new StringBuilder();
			for(Menu p : menuList){
				buildSubOption(sb,p.getId(),p.getMenuName());
				result.setData(sb.toString());
			}
		}
		return result;
	}
	
	/***
	 * 根据站点id选择分类
	 * @param siteId 站点id
	 * @return
	 */
	@RequestMapping("/getSiteCategoryById")
	public @ResponseBody
	JsonResult getSiteCategoryById(String siteId){
		JsonResult result = new JsonResult();
		List<SiteCategory> categoryList = siteService.getSiteCategoryListBySiteId(siteId);
		
		SiteCategory category = new SiteCategory();
		category.setId(0);
		category.setCategoryName("请选择");
		categoryList.add(0, category);
		if(!SuprUtil.isEmptyCollection(categoryList)){
			StringBuilder sb = new StringBuilder();
			for(SiteCategory p : categoryList){
				buildSubOption(sb,p.getId(),p.getCategoryName());
				result.setData(sb.toString());
			}
		}
		return result;
	}

	//构造下拉框
	private void buildSubOption(StringBuilder sb,Integer id,String content) {
		sb.append("<option value=");
		sb.append(id);
		sb.append(">");
		sb.append(content);
		sb.append("</option>");
	}
	
	/**
	 * 解除绑定关系
	 */
	@RequestMapping("/unbindRealtionById")
	public @ResponseBody
	Result unbindRealtionById(Integer id){
		int count = bindRelationService.unbindRealtionById(id);
		if(count==1){
			return new Result("success", Constant.DEAL_SUCCESS);
		} else {
			return new Result("error", Constant.DEAL_FAIL);
		}
	}

	/**
	 * 保存绑定关系
	 */
	@RequestMapping("/saveBindRelation")
	public @ResponseBody
	Result saveBindRelation(BindRelation bindRelation){
		try{
			if(bindRelationService.isExistBindRelation(bindRelation)){
				return new Result("fail", Constant.DEAL_FAIL);
			} else {
				bindRelationService.saveBindRelation(bindRelation);
				return new Result("success", Constant.DEAL_SUCCESS);
			}
		}catch (Exception e) {
			logger.error("绑定关系出错!"+e);
			return new Result("error", Constant.DEAL_FAIL);
		}
	}
	
	/**
	 * 栏目列表
	 */
	@RequestMapping("/getAllMenuListStore")
	public @ResponseBody
	JsonResult getAllMenuListStore(){
		JsonResult result = new JsonResult();
		List<Menu> menuList = siteService.getChildrenMenuList();
		if(SuprUtil.isEmptyCollection(menuList)){
			menuList = new ArrayList<Menu>();
		}

		Menu m = new Menu();
		m.setId(-1);
		m.setMenuName("===请选择===");
		menuList.add(0,m);
		
		result.setData(menuList);
		return result;
	}
	
	/**
	 * 站点列表
	 */
	@RequestMapping("/getAllSiteListStore")
	public @ResponseBody
	JsonResult getAllSiteListStore(){
		JsonResult result = new JsonResult();
		List<Site> siteList = siteService.getAllSiteInfoList();
		if(SuprUtil.isEmptyCollection(siteList)){
			siteList = new ArrayList<Site>();
		}
		
		Site m = new Site();
		m.setId(-1);
		m.setSiteName("===请选择===");
		siteList.add(0,m);
		
		result.setData(siteList);
		return result;
	}
	/**
	 * 站点列表
	 */
	@RequestMapping("/getCategoryNameBySiteIdStore")
	public @ResponseBody
	JsonResult getCategoryNameBySiteIdStore(String siteId){
		JsonResult result = new JsonResult();
		List<SiteCategory> categoryList = siteService.getSiteCategoryListBySiteId(siteId);
		if(SuprUtil.isEmptyCollection(categoryList)){
			categoryList = new ArrayList<SiteCategory>();
		}
		
		SiteCategory m = new SiteCategory();
		m.setId(-1);
		m.setCategoryName("===请选择===");
		categoryList.add(0,m);
		
		result.setData(categoryList);
		return result;
	}
	
}
