package com.cmge.ad.controller.admin;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmge.ad.controller.BaseController;
import com.cmge.ad.model.Result;
import com.cmge.ad.model.User;
import com.cmge.ad.service.MainService;
import com.cmge.ad.util.Constant;
import com.cmge.ad.util.JsonUtil;

/**
 * @desc 	系统管理
 * @author 	liming
 * @time 	2014-12-2
 */
@Controller
public class AdminController extends BaseController{
	
	public static final Logger logger = Logger.getLogger(AdminController.class);
	
	@Autowired
	private MainService mainService;
	
	/**
	 * 跳转首页
	 */
	@RequestMapping("/home")
	public String home(HttpSession session,ModelMap map){
		return "/adCrawl/home";
	}
	
	/**
	 * 跳转头部
	 */
	@RequestMapping("/goHeader")
	public String goHeader(HttpSession session,ModelMap map){
		return "/adCrawl/header";
	}
	
	/**
	 * 跳转登陆页
	 */
	@RequestMapping("/login")
	public String login(HttpSession session,ModelMap map){
		return "/adCrawl/test";
	}
	
	/**
	 * 跳转新增用户页
	 */
	@RequestMapping("/addUser")
	public String addUser(HttpSession session,ModelMap map){
		return "/adCrawl/user/add_user";
	}
	
	/**
	 * 新增用户
	 */
	@RequestMapping("/addUsers")
	public @ResponseBody
	Result addUsers(HttpSession session,ModelMap map,String param){
		User user = JsonUtil.getGson().fromJson(param, User.class);
		if(!mainService.exist(user)){
			mainService.addUser(user);
			return new Result("success", Constant.DEAL_SUCCESS);
		}
		return new Result("fail", Constant.DEAL_FAIL);
	}
	
}
