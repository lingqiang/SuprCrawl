package com.cmge.ad.controller;

import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cmge.ad.interfaces.req.AlbumListReq;
import com.cmge.ad.interfaces.req.ArticleListReq;
import com.cmge.ad.interfaces.req.CommentListReq;
import com.cmge.ad.interfaces.req.CommentReq;
import com.cmge.ad.interfaces.req.ConfigReq;
import com.cmge.ad.interfaces.req.FeedBackReq;
import com.cmge.ad.interfaces.req.PictureInfoReq;
import com.cmge.ad.interfaces.req.PictureListReq;
import com.cmge.ad.interfaces.req.RegisterReq;
import com.cmge.ad.interfaces.req.VersionUpdateReq;
import com.cmge.ad.interfaces.req.ZanCommentReq;
import com.cmge.ad.interfaces.req.ZanReq;
import com.cmge.ad.interfaces.resp.AlbumListResp;
import com.cmge.ad.interfaces.resp.ArticleListResp;
import com.cmge.ad.interfaces.resp.CommentListResp;
import com.cmge.ad.interfaces.resp.CommentResp;
import com.cmge.ad.interfaces.resp.ConfigResp;
import com.cmge.ad.interfaces.resp.FeedBackResp;
import com.cmge.ad.interfaces.resp.MenuListResp;
import com.cmge.ad.interfaces.resp.PictureInfoResp;
import com.cmge.ad.interfaces.resp.PictureListResp;
import com.cmge.ad.interfaces.resp.RegisterResp;
import com.cmge.ad.interfaces.resp.VersionUpdateResp;
import com.cmge.ad.interfaces.resp.ZanCommentResp;
import com.cmge.ad.interfaces.resp.ZanResp;
import com.cmge.ad.interfaces.vo.ValidateResult;
import com.cmge.ad.service.MainService;
import com.cmge.ad.util.Constant;
import com.cmge.ad.util.JsonUtil;
import com.cmge.ad.util.TimeUtils;

/**
 * @desc	接口操作类
 * @author	ljt
 * @time	2014-11-21 下午2:59:23
 */
@Controller
public class MainController {
	
	private static final Logger logger = Logger.getLogger(MainController.class);
	
	@Autowired
	private MainService mainService;
	
	/**
	 * 获取菜单列表
	 */
	@RequestMapping("/getMenuList")
	public void getMenuList(HttpServletRequest request,HttpServletResponse response){
		TimeUtils.mark();
		MenuListResp menuResp = new MenuListResp();
		try {
			// 业务处理
			menuResp = mainService.getMenuList();
		} catch (Exception e) {
			logger.error("获取菜单列表接口异常..",e);
			menuResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(menuResp);
			JsonUtil.writeString(respStr,response);
			logger.info("获取菜单列表接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	/**
	 * 获取段子
	 */
	@RequestMapping("/getArticle")
	public void getArticle(HttpServletRequest request,HttpServletResponse response,ArticleListReq articleListReq){
		TimeUtils.mark();
		ArticleListResp articleResp = new ArticleListResp();
		try {
			if(null != articleListReq){
				logger.info("获取段子接口请求："+JsonUtil.getGson().toJson(articleListReq));
				// 参数校验
				ValidateResult result = articleListReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					articleResp.setResultCode(result.getValidateResult());
					articleResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					articleResp = mainService.getArticle(articleListReq,request);
				}
			}else{
				articleResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("获取段子接口异常..",e);
			articleResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(articleResp);
			JsonUtil.writeString(respStr,response);
			logger.info("获取段子接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	
	/**
	 * 获取相册
	 */
	@RequestMapping("/getAlbum")
	public void getAlbum(HttpServletRequest request,HttpServletResponse response,AlbumListReq albumReq){
		TimeUtils.mark();
		AlbumListResp albumResp = new AlbumListResp();
		try {
			if(null != albumReq){
				logger.info("获取相册接口请求："+JsonUtil.getGson().toJson(albumReq));
				// 参数校验
				ValidateResult result = albumReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					albumResp.setResultCode(result.getValidateResult());
					albumResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					albumResp = mainService.getAlbum(albumReq,request);
				}
			}else{
				albumResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("获取相册接口异常..",e);
			albumResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(albumResp);
			JsonUtil.writeString(respStr,response);
			logger.info("获取相册接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	
	/**
	 * 获取图片
	 */
	@RequestMapping("/getPictureInfo")
	public void getPictureInfo(HttpServletRequest request,HttpServletResponse response,PictureInfoReq picReq){
		TimeUtils.mark();
		PictureInfoResp picResp = new PictureInfoResp();
		try {
			if(null != picReq){
				logger.info("获取图片接口请求："+JsonUtil.getGson().toJson(picReq));
				// 参数校验
				ValidateResult result = picReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					picResp.setResultCode(result.getValidateResult());
					picResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					picResp = mainService.getPictureInfo(picReq,request);
				}
			}else{
				picResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("获取图片接口异常..",e);
			picResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(picResp);
			JsonUtil.writeString(respStr,response);
			logger.info("获取图片接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	/**
	 * 获取图片  原有接口 不该  相册和图片一起返回
	 */
	@RequestMapping("/getPicture")
	public void getPicture(HttpServletRequest request,HttpServletResponse response,PictureListReq picReq){
		TimeUtils.mark();
		PictureListResp picResp = new PictureListResp();
		try {
			if(null != picReq){
				logger.info("获取图片接口请求："+JsonUtil.getGson().toJson(picReq));
				// 参数校验
				ValidateResult result = picReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					picResp.setResultCode(result.getValidateResult());
					picResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					picResp = mainService.getPicture(picReq,request);
				}
			}else{
				picResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("获取图片接口异常..",e);
			picResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(picResp);
			JsonUtil.writeString(respStr,response);
			logger.info("获取图片接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	/**
	 * 赞
	 */
	@RequestMapping("/zan")
	public void zan(HttpServletRequest request,HttpServletResponse response,ZanReq zanReq){
		TimeUtils.mark();
		ZanResp zanResp = new ZanResp();
		try {
			if(null != zanReq){
				logger.info("赞接口请求："+JsonUtil.getGson().toJson(zanReq));
				// 参数校验
				ValidateResult result = zanReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					zanResp.setResultCode(result.getValidateResult());
					zanResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					zanResp = mainService.zan(zanReq);
				}
			}else{
				zanResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("赞接口异常..",e);
			zanResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(zanResp);
			JsonUtil.writeString(respStr,response);
			logger.info("赞接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	/**
	 * 版本更新
	 */
	@RequestMapping("/versionUpdate")
	public void versionUpdate(HttpServletRequest request,HttpServletResponse response,VersionUpdateReq versionReq){
		TimeUtils.mark();
		VersionUpdateResp versionResp = new VersionUpdateResp();
		try {
			if(null != versionReq){
				logger.info("版本更新接口请求："+JsonUtil.getGson().toJson(versionReq));
				// 参数校验
				ValidateResult result = versionReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					versionResp.setResultCode(result.getValidateResult());
					versionResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					versionResp = mainService.versionUpdate(versionReq);
				}
			}else{
				versionResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("版本更新接口异常..",e);
			versionResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(versionResp);
			JsonUtil.writeString(respStr,response);
			logger.info("版本更新接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	/**
	 * 意见反馈
	 */
	@RequestMapping("/feedBack")
	public void feedBack(HttpServletRequest request,HttpServletResponse response,FeedBackReq feedBackReq){
		TimeUtils.mark();
		FeedBackResp feeBackResp = new FeedBackResp();
		try {
			if(null != feedBackReq){
				feedBackReq.setContent(URLDecoder.decode(feedBackReq.getContent(), "utf-8"));
				logger.info("意见反馈接口请求："+JsonUtil.getGson().toJson(feedBackReq));
				// 参数校验
				ValidateResult result = feedBackReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					feeBackResp.setResultCode(result.getValidateResult());
					feeBackResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					feeBackResp = mainService.feedBack(feedBackReq);
				}
			}else{
				feeBackResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("意见反馈接口异常..",e);
			feeBackResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(feeBackResp);
			JsonUtil.writeString(respStr,response);
			logger.info("意见反馈接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	
	/**
	 * 获取系统配置项值
	 */
	@RequestMapping("/getConfigValue")
	public void getConfigValue(HttpServletRequest request,HttpServletResponse response,ConfigReq configReq){
		TimeUtils.mark();
		ConfigResp configResp = new ConfigResp();
		try {
			if(null != configReq){
				logger.info("获取系统配置项值接口请求："+JsonUtil.getGson().toJson(configReq));
				// 参数校验
				ValidateResult result = configReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					configResp.setResultCode(result.getValidateResult());
					configResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					configResp = mainService.getConfigValue(configReq);
				}
			}else{
				configResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("获取系统配置项值接口异常..",e);
			configResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(configResp);
			JsonUtil.writeString(respStr,response);
			logger.info("获取系统配置项值接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	/**
	 * 注册接口
	 */
	@RequestMapping("/register")
	public void register(HttpServletRequest request,HttpServletResponse response,RegisterReq registerReq){
		TimeUtils.mark();
		RegisterResp registerResp = new RegisterResp();
		try {
			if(null != registerReq){
				logger.info("注册接口请求："+JsonUtil.getGson().toJson(registerReq));
				// 参数校验
				ValidateResult result = registerReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					registerResp.setResultCode(result.getValidateResult());
					registerResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					registerResp = mainService.register(registerReq);
				}
			}else{
				registerResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("注册接口异常..",e);
			registerResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(registerResp);
			JsonUtil.writeString(respStr,response);
			logger.info("注册接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	
	/**
	 * 评论赞接口
	 */
	@RequestMapping("/zan/comment")
	public void zanComment(HttpServletRequest request,HttpServletResponse response,ZanCommentReq zanReq){
		TimeUtils.mark();
		ZanCommentResp zanResp = new ZanCommentResp();
		try {
			if(null != zanReq){
				logger.info("评论赞接口请求："+JsonUtil.getGson().toJson(zanReq));
				// 参数校验
				ValidateResult result = zanReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					zanResp.setResultCode(result.getValidateResult());
					zanResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					zanResp = mainService.zanComment(zanReq);
				}
			}else{
				zanResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("评论赞接口异常..",e);
			zanResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(zanResp);
			JsonUtil.writeString(respStr,response);
			logger.info("评论赞接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	/**
	 * 评论接口
	 */
	@RequestMapping("/comment")
	public void comment(HttpServletRequest request,HttpServletResponse response,CommentReq commentReq){
		TimeUtils.mark();
		CommentResp commentResp = new CommentResp();
		try {
			if(null != commentReq){
				logger.info("评论接口请求："+JsonUtil.getGson().toJson(commentReq));
				// 参数校验
				ValidateResult result = commentReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					commentResp.setResultCode(result.getValidateResult());
					commentResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					commentResp = mainService.comment(commentReq);
				}
			}else{
				commentResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("评论接口异常..",e);
			commentResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(commentResp);
			JsonUtil.writeString(respStr,response);
			logger.info("评论接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
	/**
	 * 评论列表接口
	 */
	@RequestMapping("/getCommentList")
	public void getCommentList(HttpServletRequest request,HttpServletResponse response,CommentListReq commentReq){
		TimeUtils.mark();
		CommentListResp commentResp = new CommentListResp();
		try {
			if(null != commentReq){
				logger.info("评论列表接口请求："+JsonUtil.getGson().toJson(commentReq));
				// 参数校验
				ValidateResult result = commentReq.validate();
				if (result.getValidateResult() != Constant.DEAL_SUCCESS) {
					commentResp.setResultCode(result.getValidateResult());
					commentResp.setErrMsg(result.getErrorInfo());
				} else {
					// 业务处理
					commentResp = mainService.getCommentList(commentReq);
				}
			}else{
				commentResp.setResultCode(Constant.PARAM_IS_NULL);
			}
		} catch (Exception e) {
			logger.error("评论列表接口异常..",e);
			commentResp.setResultCode(Constant.DEAL_EXCEPTION);
		} finally{
			String respStr = JsonUtil.toJson(commentResp);
			JsonUtil.writeString(respStr,response);
			logger.info("评论列表接口响应["+TimeUtils.get()+"ms]："+respStr);
		}
	}
	
}
