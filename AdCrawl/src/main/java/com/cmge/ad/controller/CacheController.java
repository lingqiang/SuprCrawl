package com.cmge.ad.controller;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cmge.ad.cache.CacheManagerI;
import com.cmge.ad.util.Constant;
import com.cmge.ad.util.TimeUtils;

/**
 * @desc	缓存操作类
 * @author	ljt
 * @time	2014-11-21 下午2:59:23
 */
@Controller
public class CacheController {
	
	private static final Logger logger = Logger.getLogger(CacheController.class);
	
	@Autowired
	private CacheManagerI cacheManager;
	
	/**
	 * 查询缓存
	 * @param region
	 * @param key
	 * @param request
	 * @param response
	 */
	@RequestMapping("/get/{region}/{key}")
	public void getCache(@PathVariable String region,@PathVariable String key,HttpServletRequest request,HttpServletResponse response){
		TimeUtils.mark();
		Object obj = null;
		ObjectOutputStream oos = null;
		try {
			obj = cacheManager.get(region, key);
			ServletOutputStream bos = response.getOutputStream();
			oos = new ObjectOutputStream(bos);
			oos.writeObject(obj);
			oos.flush();
		} catch (Exception e) {
			logger.error("查询缓存接口异常..",e);
		} finally{
			IOUtils.closeQuietly(oos);
			logger.info("查询"+region+":"+key+"缓存接口响应["+TimeUtils.get()+"ms]["+(obj==null ? '没' : '已')+"]缓存");
		}
	}

	
	/**
	 * 新增缓存
	 * @param region
	 * @param key
	 * @param request
	 * @param response
	 */
	@RequestMapping("/add/{region}/{key}")
	public void addCache(@PathVariable String region,@PathVariable String key,HttpServletRequest request,HttpServletResponse response){
		TimeUtils.mark();
		Object obj = null;
		ObjectInputStream oin = null;
		try {
			InputStream in = request.getInputStream();
            oin = new ObjectInputStream(in);
            obj = oin.readObject();
            if(null != obj){
            	cacheManager.put(region, key,obj);
            }
		} catch (Exception e) {
			logger.error("新增缓存接口异常..",e);
		} finally{
			IOUtils.closeQuietly(oin);
			logger.info("新增"+region+":"+key+"缓存接口响应["+TimeUtils.get()+"ms]");
		}
	}
	
	/**
	 * 更新缓存
	 * @param region
	 * @param key
	 * @param request
	 * @param response
	 */
	@RequestMapping("/update/{region}/{key}")
	public void updateCache(@PathVariable String region,@PathVariable String key,HttpServletRequest request,HttpServletResponse response){
		TimeUtils.mark();
		Object obj = null;
		ObjectInputStream oin = null;
		try {
			InputStream in = request.getInputStream();
            oin = new ObjectInputStream(in);
            obj = oin.readObject();
            if(null != obj){ 
            	cacheManager.update(region, key,obj);
            }
		} catch (Exception e) {
			logger.error("更新缓存接口异常..",e);
		} finally{
			IOUtils.closeQuietly(oin);
			logger.info("更新"+region+":"+key+"缓存接口响应["+TimeUtils.get()+"ms]");
		}
	}
	
	
	/**
	 * 删除缓存
	 * @param region
	 * @param key
	 * @param request
	 * @param response
	 */
	@RequestMapping("/delete/{region}/{key}")
	public void deleteCache(@PathVariable String region,@PathVariable String key,HttpServletRequest request,HttpServletResponse response){
		TimeUtils.mark();
		try {
			cacheManager.delete(region, key);
			response.getWriter().write("delete "+region+":"+key+" cache success["+TimeUtils.get()+"ms]");
		} catch (Exception e) {
			logger.error("删除缓存接口异常..",e);
		} finally{
			logger.info("删除"+region+":"+key+"缓存接口响应["+TimeUtils.get()+"ms]");
		}
	}
	
	/**
	 * 清除某个块的缓存
	 * @param region
	 * @param key
	 * @param request
	 * @param response
	 */
	@RequestMapping("/delete/{region}")
	public void deleteRegionCache(@PathVariable String region,HttpServletRequest request,HttpServletResponse response){
		TimeUtils.mark();
		try {
			cacheManager.clearRegionCache(region);
			response.getWriter().write("deleteRegionCache success["+TimeUtils.get()+"ms]");
		} catch (Exception e) {
			logger.error("删除缓存块接口异常..",e);
		} finally{
			logger.info("删除缓存块接口响应["+TimeUtils.get()+"ms]");
		}
	}
	
	/**
	 * 缓存心跳
	 * @param response
	 */
	@RequestMapping("/monitor")
	public void monitor(HttpServletResponse response){
		TimeUtils.mark();
		Integer ret = Constant.DEAL_SUCCESS;
		ObjectOutputStream oos = null;
		try {
			ServletOutputStream bos = response.getOutputStream();
			oos = new ObjectOutputStream(bos);
			oos.writeObject(ret);
			oos.flush();
		} catch (Exception e) {
			logger.error("缓存心跳接口异常..",e);
		} finally{
			IOUtils.closeQuietly(oos);
			logger.info("缓存心跳接口响应["+TimeUtils.get()+"ms]");
		}
	}
	
}
