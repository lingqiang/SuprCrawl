package com.cmge.ad.controller;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

import com.cmge.ad.model.Admin;
import com.cmge.ad.util.Constant;
import com.cmge.ad.util.Pager;

/**
 * @desc	基类控制器
 * @author	ljt
 * @time	2014-10-14 下午6:05:18
 */
public class BaseController {
	
	public static final Logger logger = Logger.getLogger(BaseController.class);
	
	/**
	 * 分页 每页显示数量
	 */
	public int pageSize = Pager.PAGE_SIZE;
	
	public Admin getSessionUser(HttpSession session) {
		Admin admin = (Admin) session.getAttribute(Constant.USERINFO);
		return admin;
	}
	/**
	 * 分页 当前页数
	 */
	public int pageNum;
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	
	/**
	 * 提供日志打印对象
	 * @return
	 */
	public Logger Logger(){
		return Logger.getLogger(super.getClass());
	} 

}
