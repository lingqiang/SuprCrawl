package com.cmge.ad.controller.site;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmge.ad.controller.BaseController;
import com.cmge.ad.model.JsonResult;
import com.cmge.ad.model.Menu;
import com.cmge.ad.model.Result;
import com.cmge.ad.model.Site;
import com.cmge.ad.model.SiteCategory;
import com.cmge.ad.service.SiteService;
import com.cmge.ad.util.Constant;
import com.cmge.ad.util.Pager;
import com.cmge.ad.util.SuprUtil;

/**
 * @desc	站点、分类管理
 * @author	ljt
 * @time	2015-1-27 下午4:40:18
 */
@Controller
public class SiteController extends BaseController{
	
	public static final Logger logger = Logger.getLogger(SiteController.class);
	
	@Autowired
	private SiteService siteService;
	
	/**
	 * 站点列表
	 */
	@RequestMapping("/getAllSiteList")
	public @ResponseBody
	JsonResult getAllSiteList(int type){
		JsonResult result = new JsonResult();
		List<Site> siteList = siteService.getAllSiteList(type);
		if(SuprUtil.isEmptyCollection(siteList)){
			siteList = new ArrayList<Site>();
		}

		Site site = new Site();
		site.setId(-1);
		site.setSiteName("===请选择===");
		siteList.add(0,site);
		
		result.setData(siteList);
		return result;
	}
	
	/**
	 * 站点列表
	 */
	@RequestMapping("/getSiteListByType")
	public @ResponseBody
	JsonResult getSiteListByType(int type){
		JsonResult result = new JsonResult();
		StringBuilder sb = new StringBuilder();
		
		List<Site> siteList = siteService.getAllSiteList(type);
		if(SuprUtil.isEmptyCollection(siteList)){
			siteList = new ArrayList<Site>();
		}

		Site site = new Site();
		site.setId(-1);
		site.setSiteName("=======请选择=======");
		siteList.add(0,site);
		
		for (Site s : siteList) {
			buildSubOption(sb, s.getId(), s.getSiteName());
		}
		
		result.setData(sb.toString());
		return result;
	}
	
	private void buildSubOption(StringBuilder sb, Integer id, String content) {
		sb.append("<option value=");
		sb.append(id);
		sb.append(">");
		sb.append(content);
		sb.append("</option>");
	}
	
	
	/**
	 * 菜单列表
	 */
	@RequestMapping("/getParentMenu")
	public @ResponseBody
	JsonResult getParentMenu(){
		JsonResult result = new JsonResult();
		List<Menu> menuList = siteService.getParentMenu();
		if(SuprUtil.isEmptyCollection(menuList)){
			menuList = new ArrayList<Menu>();
		}

		Menu menu = new Menu();
		menu.setId(-1);
		menu.setMenuName("===请选择===");
		menuList.add(0,menu);
		
		result.setData(menuList);
		return result;
	}
	
	
	/**
	 * 根据站点id获取类型列表
	 */
	@RequestMapping("/getCategoryBySiteId")
	public @ResponseBody
	JsonResult getCategoryBySiteId(String siteId){
		JsonResult result = new JsonResult();
		List<SiteCategory> categoryList = siteService.getCategoryBySiteId(siteId);
		if(SuprUtil.isEmptyCollection(categoryList)){
			categoryList = new ArrayList<SiteCategory>();
		}

		SiteCategory category = new SiteCategory();
		category.setId(-1);
		category.setCategoryName("===请选择===");
		categoryList.add(0,category);
		
		result.setData(categoryList);
		return result;
	}
	
	@RequestMapping("/getCategoryListBySiteId")
	public @ResponseBody
	JsonResult getCategoryListBySiteId(String siteId){
		JsonResult result = new JsonResult();
		StringBuilder sb = new StringBuilder();
		
		List<SiteCategory> categoryList = siteService.getCategoryBySiteId(siteId);
		if(SuprUtil.isEmptyCollection(categoryList)){
			categoryList = new ArrayList<SiteCategory>();
		}

		SiteCategory category = new SiteCategory();
		category.setId(-1);
		category.setCategoryName("=======请选择=======");
		categoryList.add(0,category);
		
		for (SiteCategory c : categoryList) {
			buildSubOption(sb, c.getId(), c.getCategoryName());
		}
		
		result.setData(sb.toString());
		return result;
	}
	
	/**
	 * 跳转站点列表
	 */
	@RequestMapping("/siteList")
	public String siteList(){
		return "/adCrawl/site/site_list";
	}
	
	/**
	 * 跳转站点分类列表
	 */
	@RequestMapping("/siteCategoryList")
	public String siteCategoryList(){
		return "/adCrawl/site/site_category_list";
	}
	
	/**
	 * 跳转菜单列表
	 */
	@RequestMapping("/menuList")
	public String menuList(){
		return "/adCrawl/menu/menu_list";
	}
	
	/**
	 * 跳转新增子菜单列表
	 */
	@RequestMapping("/addChildMenuBefore")
	public String addChildMenuBefore(ModelMap map){
		// 获取一级菜单列表
		List<Menu> menuList = siteService.getParentMenu();
		map.addAttribute("menuList", menuList);
		return "/adCrawl/menu/add_child_menu";
	}
	
	/**
	 * 新增子菜单
	 */
	@RequestMapping("/addChildMenu")
	public @ResponseBody
	Result addChildMenu(Menu menu){
		if(!siteService.existMenu(menu)){
			siteService.addChildMenu(menu);
			return new Result("success", Constant.DEAL_SUCCESS);
		}else{
			return new Result("fail", Constant.DEAL_FAIL);
		}
	}
	
	/**
	 * 删除子菜单
	 */
	@RequestMapping("/deleteChildMenuById")
	public @ResponseBody
	Result deleteChildMenuById(Integer id){
		if(!siteService.existMenuRelation(id)){
			siteService.deleteChildMenuById(id);
			return new Result("success", Constant.DEAL_SUCCESS);
		}else{
			return new Result("fail", Constant.DEAL_FAIL);
		}
	}
	
	
	/**
	 * 菜单列表
	 */
	@RequestMapping("/getMenusList")
	public @ResponseBody
	JsonResult getMenusList(HttpSession session,Pager<Menu> pager,Menu menu) throws UnsupportedEncodingException {
		JsonResult result = new JsonResult();
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		
		if(!StringUtils.isEmpty(menu.getKey())){
			menu.setKey(URLDecoder.decode(menu.getKey(), "utf-8"));
		}
		
		paramMap.put("menu", menu);
		siteService.getMenuList(pager,paramMap);
		result.setData(pager.getRows());
		result.setTotalCount(pager.getTotalCount());
		return result;
	}
	
	/**
	 * 站点列表
	 */
	@RequestMapping("/getSiteList")
	public @ResponseBody
	JsonResult getSiteList(HttpSession session,Pager<Site> pager,Site site) throws UnsupportedEncodingException {
		JsonResult result = new JsonResult();
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		
		if(!StringUtils.isEmpty(site.getKey())){
			site.setKey(URLDecoder.decode(site.getKey(), "utf-8"));
		}
		
		paramMap.put("site", site);
		siteService.getSiteList(pager,paramMap);
		result.setData(pager.getRows());
		result.setTotalCount(pager.getTotalCount());
		return result;
	}
	
	/**
	 * 站点分类列表
	 */
	@RequestMapping("/getSiteCategoryList")
	public @ResponseBody
	JsonResult getSiteCategoryList(HttpSession session,Pager<SiteCategory> pager,SiteCategory site) throws UnsupportedEncodingException {
		JsonResult result = new JsonResult();
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		
		if(!StringUtils.isEmpty(site.getKey())){
			site.setKey(URLDecoder.decode(site.getKey(), "utf-8"));
		}
		
		paramMap.put("site", site);
		siteService.getSiteCategoryList(pager,paramMap);
		result.setData(pager.getRows());
		result.setTotalCount(pager.getTotalCount());
		return result;
	}
	
}
