package com.cmge.ad.util.thread;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.cmge.ad.service.PictureService;
import com.cmge.ad.util.context.SpringContextUtil;

/**
 * @desc	同步相册
 * @author	ljt
 * @time	2015-1-29 下午3:50:46
 */
public class SyncAlbum implements Runnable {

	public static final Logger logger = Logger.getLogger(SyncAlbum.class);
	
	private PictureService pictureService;
	
	public void run() {
		ApplicationContext context = SpringContextUtil.getApplicationContext();
		pictureService = (PictureService) context.getBean(PictureService.class);
		
		// 10分钟每个分类同步一个相册
		while (true) {
			try {
				logger.info("同步相册开始....");
				
				pictureService.syncAlbum();
				
				logger.info("同步相册结束....");
				
				// 休眠半分钟
				Thread.sleep(1000*60);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			
		}
	}

}
