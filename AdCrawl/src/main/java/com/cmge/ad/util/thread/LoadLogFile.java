package com.cmge.ad.util.thread;

import java.io.File;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.cmge.ad.util.TimeUtils;

/**
 * @desc 	动态加载Log4j.xml
 * @author 	ljt
 * @time 	2014-9-11 下午3:34:35
 */
public class LoadLogFile implements Runnable {

	public static final Logger logger = Logger.getLogger(LoadLogFile.class);

	// 前一次更新时间
	private String beforeTime = "";

	private String log4jPath;

	public LoadLogFile(String log4jPath) {
		super();
		this.log4jPath = log4jPath;
	}

	public void run() {
		while (true) {
			try {
				// 文件最新修改时间
				String modifiTime = "";
				File f = new File(log4jPath);
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(f.lastModified());
				modifiTime = TimeUtils.formatTimeToStr(cal.getTime());

				if (!modifiTime.equals(beforeTime)) {
					// 配置log4j
					DOMConfigurator.configure(log4jPath);
					logger.info("动态加载log4j.xml配置成功....");
					beforeTime = modifiTime;
				}
			} catch (Exception e) {
				logger.error("DynamicLoadLogFileQuartz定时任务出错!", e);
			} finally {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
				}
			}
		}
	}

}
