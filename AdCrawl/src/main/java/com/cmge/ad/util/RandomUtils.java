package com.cmge.ad.util;

import java.util.Random;

public class RandomUtils {

	/**
	 * 长度
	 */
	public static final int LENGTH = 12;

	/**
	 * 产生一个固定长度的随机字符串
	 * 
	 * @param length
	 * @return
	 */
	public static String getRandomStr(int length) {
		String str = "abcdefghigklmnopkrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();
		StringBuffer sf = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(62);// 0~61
			sf.append(str.charAt(number));

		}
		return sf.toString();
	}

	/**
	 * 产生一个固定长度的随机字符串
	 * 
	 * @param length
	 * @return
	 */
	public static String getRandomStr() {
		return getRandomStr(LENGTH);
	}

}
