package com.cmge.ad.util.thread;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.cmge.ad.service.ArticleService;
import com.cmge.ad.util.context.SpringContextUtil;

/**
 * @desc	同步段子
 * @author	ljt
 * @time	2015-1-29 下午3:50:46
 */
public class SyncArticle implements Runnable {

	public static final Logger logger = Logger.getLogger(SyncArticle.class);
	
	private ArticleService articleService;
	
	private int size = 10;
	
	public void run() {
		ApplicationContext context = SpringContextUtil.getApplicationContext();
		articleService = (ArticleService) context.getBean(ArticleService.class);
		
		while (true) {
			try {
				logger.info("同步段子开始....");
				
				articleService.syncArticle(size);
				
				logger.info("同步段子结束....");
				
				// 休眠30秒
				Thread.sleep(30000);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			
		}
	}

}
