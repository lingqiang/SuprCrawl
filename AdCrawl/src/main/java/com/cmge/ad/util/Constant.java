package com.cmge.ad.util;

/**
 * @功能：	常量类
 * @作者： 	ljt
 * @时间： 	2014-4-30 上午10:16:01
 */
public class Constant {
	
	/** 处理成功 **/
	public static final int DEAL_SUCCESS = 0;
	
	/** 处理失败 **/
	public static final int DEAL_FAIL = -1;
	
	/** 处理异常 **/
	public static final int DEAL_EXCEPTION = -2;
	
	/** 解析参数异常 **/
	public static final int PARSE_PARAM_EXCEPTION = -3;
	
	/** 参数校验错误 **/
	public static final int PARAM_VALIDATE_ERROR = -4;
	
	/** 参数校验异常 **/
	public static final int PARAM_VALIDATE_EXCEPTION = -5;
	
	/** 参数为空 **/
	public static final int PARAM_IS_NULL = -6;
	
	/** 新增缓存操作码  **/
	public static final String ADD_CACHE = "add";
	
	/** 删除缓存操作码  **/
	public static final String DELETE_CACHE = "delete";
	
	/** 更新缓存操作码  **/
	public static final String UPDATE_CACHE = "update";
	
	/** 查询缓存操作码  **/
	public static final String GET_CACHE = "get";
	
	/** 用户缓存名称  **/
	public static final String USER_CACHE = "UserCache";
	
	/** 设备号+imsi 用户缓存名称  **/
	public static final String DEVICE_IMSI_USER_CACHE = "DeviceImsiUserCache";
	
	/** 用户已安装应用名缓存名称 **/
	public static final String USER_APP_CACHE = "UserAppCache";
	
	/** 广告横表按类型缓存名称  **/
	public static final String AD_TRADE_CACHE = "AdTradeCache";
	
	/** 广告模型数据缓存名称  **/
	public static final String AD_MOULD_CACHE = "Mould";
	
	/** 广告资源数据缓存名称  **/
	public static final String AD_RESOURCE_CACHE = "Resource";

	/** 广告供应商数据缓存名称  **/
	public static final String AD_SUPPLIER_CACHE = "SupplierService";
	
	/** 用户缓存时间更新标识   不需要同步 **/
	public static final int USER_TIME_NOT_SYNC = 1;
	
	/** 用户缓存时间更新标识   需要同步 **/
	public static final int USER_TIME_NEED_SYNC = 2;
	
	/** 用户缓存时间更新标识   已同步 **/
	public static final int USER_TIME_ALREADY_SYNC = 3;

	/** session中存放用户信息的key **/
	public static final String USERINFO = "USERINFO";
	
}
