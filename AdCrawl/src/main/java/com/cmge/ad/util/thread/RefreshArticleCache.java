package com.cmge.ad.util.thread;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.cmge.ad.service.ArticleService;
import com.cmge.ad.util.context.SpringContextUtil;

/**
 * @desc	刷新段子缓存  暂时没用
 * @author	ljt
 * @time	2015-1-29 下午3:50:46
 */
public class RefreshArticleCache implements Runnable {

	public static final Logger logger = Logger.getLogger(RefreshArticleCache.class);
	
	private ArticleService articleService;
	
	public void run() {
		ApplicationContext context = SpringContextUtil.getApplicationContext();
		articleService = (ArticleService) context.getBean(ArticleService.class);
		
		while (true) {
			logger.info("刷新段子缓存开始....");
			
			articleService.refreshArticleCache();
			
			logger.info("刷新段子缓存结束....");
			
			try {
				// 休眠10分钟
				Thread.sleep(60000*10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}

}
