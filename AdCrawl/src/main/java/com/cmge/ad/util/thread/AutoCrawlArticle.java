package com.cmge.ad.util.thread;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.cmge.ad.service.CrawlService;
import com.cmge.ad.util.context.SpringContextUtil;

/**
 * @desc	自动抓取段子
 * @author	ljt
 * @time	2015-1-29 下午3:50:46
 */
public class AutoCrawlArticle implements Runnable {

	public static final Logger logger = Logger.getLogger(AutoCrawlArticle.class);
	
	private CrawlService crawlService;
	
	public void run() {
		ApplicationContext context = SpringContextUtil.getApplicationContext();
		crawlService = (CrawlService) context.getBean(CrawlService.class);
		
		while (true) {
			try {
				logger.info("自动抓取段子开始....");
				
				crawlService.autoCrawlArticle();
				
				logger.info("自动抓取段子结束....");
				
				// 休眠30分钟
				Thread.sleep(60000*30);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			
		}
	}

}
