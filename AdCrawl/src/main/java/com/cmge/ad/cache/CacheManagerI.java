package com.cmge.ad.cache;

/**
 * @desc	缓存管理器顶级接口
 * @author	ljt
 * @time	2014-11-21 下午2:12:58
 */
public interface CacheManagerI {
	
	public void init();

    public void shutdown();

    public Object get(Object key);
    
    public Object get(String region, Object key);

    public Boolean replace(Object key, Object value);

    public Boolean replace(String region, Object key, Object value);
    
    public void update(String region, Object key, Object value);

    public void delete(Object key);

    public void delete(String region, Object key);

    public void put(Object key, Object value);

    public void put(String region, Object key, Object value);

    public Boolean putIfAbsent(Object key, Object value);

    public Boolean putIfAbsent(String region, Object key, Object value);

    public void clearAll();

    public void clearRegion(String region);
    
    public void clearRegionCache(String region);

    public void setCacheConfig(CacheConfig cacheConfig);
    
    public CacheConfig getCacheConfig();
}


