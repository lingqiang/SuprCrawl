package com.cmge.ad.cache.redis;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;

/**
 * @desc	Redis缓存实现 操作列表
 * 
 * 			后续加上Key生成策略，不同接口配置不同的Key生成策略，读取配置后自动组装Key查询缓存
 * 
 * @author	ljt
 * @time	2014-8-28 下午6:32:17
 */
//@Component
//@Lazy(value = false)
public class RedisCache{

	private static final Logger logger = Logger.getLogger(RedisCache.class);

//	@Resource(name="redisTemplate")
	private RedisTemplate<String,String> redisTemplate;
	
	/**
	 * list操作类
	 */
//	@Resource(name="redisTemplate")
	private ListOperations<String,String> listOperations;
	
	private SetOperations<String,String> setOperations;
	
	private ValueOperations<String,String> valueOperations;
	
	private ZSetOperations<String,String> zSetOperations;
	
	private HashOperations<String,String,String> hashOperations;

	public void add(String key, String value){
		try {
			// 从右边添加元素
			listOperations.rightPush(key, value);
		} catch (Exception e) {
			logger.error("add cache exception,key is " + key + ",value is " + value, e);
			throw new RedisCacheException(e);
		}
	}
	
	public String remove(String key){
		String result = null;
		try {
			// 从左边移除元素 并且返回元素值
			result = redisTemplate.opsForList().leftPop(key);
		} catch (Exception e) {
			logger.error("remove cache exception,key is " + key, e);
			throw new RedisCacheException(e);
		}
		return result;
	}
	
	public void delete(String key) {
		try {
			redisTemplate.delete(key);
		} catch (Exception e) {
			logger.error("delete cache exception,key is " + key, e);
			throw new RedisCacheException(e);
		}
	}

	/**
	 * 根据key获取list集合
	 */
	public List<String> get(String key) {
		List<String> list = null;
		try {
			list = listOperations.range(key, 0, Integer.MAX_VALUE);
		} catch (Exception e) {
			logger.error("get key list exception,key is "+key, e);
			throw new RedisCacheException(e);
		}
		return list;
	}

	/**
	 * 获取list分页
	 */
	public List<String> get(String key, int start, int end) {
		List<String> list = null;
		try {
			list = listOperations.range(key, start, end);
		} catch (Exception e) {
			logger.error("get key pager list exception,key is "+key+",start is "+start+",end is "+end, e);
			throw new RedisCacheException(e);
		}
		return list;
	}

	public RedisTemplate<String, String> getRedisTemplate() {
		return redisTemplate;
	}

	public void setRedisTemplate(RedisTemplate<String, String> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public ListOperations<String, String> getListOperations() {
		return listOperations;
	}

	public void setListOperations(ListOperations<String, String> listOperations) {
		this.listOperations = listOperations;
	}

	public SetOperations<String, String> getSetOperations() {
		return setOperations;
	}

	public void setSetOperations(SetOperations<String, String> setOperations) {
		this.setOperations = setOperations;
	}

	public ValueOperations<String, String> getValueOperations() {
		return valueOperations;
	}

	public void setValueOperations(ValueOperations<String, String> valueOperations) {
		this.valueOperations = valueOperations;
	}

	public ZSetOperations<String, String> getzSetOperations() {
		return zSetOperations;
	}

	public void setzSetOperations(ZSetOperations<String, String> zSetOperations) {
		this.zSetOperations = zSetOperations;
	}

	public HashOperations<String, String, String> getHashOperations() {
		return hashOperations;
	}

	public void setHashOperations(HashOperations<String, String, String> hashOperations) {
		this.hashOperations = hashOperations;
	}
}
