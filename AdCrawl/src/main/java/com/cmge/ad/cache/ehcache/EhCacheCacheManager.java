package com.cmge.ad.cache.ehcache;

import java.net.URL;
import java.util.ArrayList;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.springframework.stereotype.Service;

import com.cmge.ad.cache.AbstractCacheManager;
import com.cmge.ad.cache.CacheConfig;

/**
 * @desc	EhCache缓存管理器
 * @author	ljt
 * @time	2014-11-21 下午1:57:11
 */
@Service("cacheManager")
public class EhCacheCacheManager extends AbstractCacheManager{
	
	private CacheManager ehcache;
	
	/**
	 * 缓存配置类  暂时用不到
	 */
	private CacheConfig cacheConfig;
	
	private EhCacheCacheManager(){
		synchronized (EhCacheCacheManager.class) {
			init();
		}
	}

	public CacheConfig getCacheConfig() {
		return cacheConfig;
	}
	
	public void setCacheConfig(CacheConfig cacheConfig) {
		this.cacheConfig = cacheConfig;
	}
	
	public CacheManager getEhCache(){
		return ehcache;
	}
	
	/**
	 * EhCache缓存初始化
	 */
	public void init() {
		URL url = EhCacheCacheManager.class.getResource("/cache/ehcache_single.xml");
		ehcache = CacheManager.create(url);
	}
	
	/**
	 * EhCache缓存销毁
	 */
	public void shutdown() {
		ehcache.shutdown();
		ehcache = null;
	}
	
	/**
	 * 获取某个块某个key的缓存对象
	 */
	public Object get(String region, Object key) {
        Ehcache cache = getEhcacheByRegion(region);
        Element element = cache.get(key);
        if (element != null) {
            return element.getObjectValue();
        }
        return null;
    }
	
	/**
	 * 获取某个块所有key值
	 * @param region
	 * @return
	 */
	public ArrayList<String> getAllKeys(String region){
		Ehcache cache = getEhcacheByRegion(region);
		ArrayList<String> list = (ArrayList<String>) cache.getKeys();
		return list;
	}

	/**
	 * 删除某个块某个key对应的缓存
	 */
    public void delete(String region, Object key) {
        Ehcache cache = getEhcacheByRegion(region);
        cache.remove(key);
    }

    /**
     * 新增某个块某个key对应缓存对象value
     */
    public void put(String region, Object key, Object value) {
        Ehcache cache = getEhcacheByRegion(region);
        cache.put(new Element(key, value));
    }

    /**
     * 替换缓存值  只有在key值缓存事先存在返回true
     */
    public Boolean replace(String region, Object key, Object value) {
        Ehcache cache = getEhcacheByRegion(region);
        Element replace = cache.replace(new Element(key, value));
        if (replace == null) {
            return false;
        }
        return true;
    }
    
    /**
     * 不推荐使用
     */
    public void update(String region, Object key, Object value) {
    	delete(region,key);
    	put(region, key, value);
    }

    /**
     * 如果不存在则设置 否则不处理
     */
    public Boolean putIfAbsent(String region, Object key, Object value) {
        Ehcache cache = getEhcacheByRegion(region);
        Element replace = cache.putIfAbsent(new Element(key, value));
        if (replace == null) {
            return true;
        }
        return false;
    }

    /**
     * 清除全部缓存
     */
    public void clearAll() {
        ehcache.removeAllCaches();
    }

    /**
     * 清空缓存块
     */
    public void clearRegion(String region) {
        ehcache.removeCache(region);
    }
    
    /**
     * 清空缓存块中所有元素
     */
    public void clearRegionCache(String region) {
    	if (region == null) {
            throw new NullPointerException(REGION_CANT_BE_NULL);
        }

        Ehcache cache = ehcache.getEhcache(region);
        if (cache != null) {
        	cache.removeAll();
        }
    }

    /**
     * 获取缓存块
     */
    private Ehcache getEhcacheByRegion(String region) {
        if (region == null) {
            throw new NullPointerException(REGION_CANT_BE_NULL);
        }

        Ehcache cache = ehcache.getEhcache(region);
        if (cache == null) {
            ehcache.addCache(region);
            cache = ehcache.getEhcache(region);
        }
        return cache;
    }
    
}
