package com.cmge.ad.cache;


/**
 * @desc	缓存工厂 用于获取缓存管理器
 * @author	ljt
 * @time	2014-11-21 下午2:08:31
 */
public class CacheManagerFactory {
	
	private static CacheManagerFactory cacheManagerFactory;
	
	private CacheManagerFactory(){}
	
	/**
	 * 从这里获取缓存管理器
	 */
	public CacheManagerI createCacheManager(CacheConfig cacheConfig){
		return null;
	}
	
	public static CacheManagerFactory getInstance(){
		if(null == cacheManagerFactory){
			cacheManagerFactory = new CacheManagerFactory();
		}
		return cacheManagerFactory;
	}
	
}
