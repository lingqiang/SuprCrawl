package com.cmge.ad.model;

import java.util.HashSet;

/**
 * @desc	抓取相册对象
 * @author	ljt
 * @time	2015-2-3 下午4:56:01
 */
public class CrawlAlbum {
	
	// 相册唯一Id
	private String uniqueId;
	
	// 相册标题
	private String albumTitle;
	
	// 图片url  用set自动去重
	private HashSet<String> picList;
	
	// 图片详情页url  用set自动去重
	private HashSet<String> picInfoList;
	
	public HashSet<String> getPicInfoList() {
		return picInfoList;
	}

	public void setPicInfoList(HashSet<String> picInfoList) {
		this.picInfoList = picInfoList;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public HashSet<String> getPicList() {
		return picList;
	}

	public void setPicList(HashSet<String> picList) {
		this.picList = picList;
	}
}
