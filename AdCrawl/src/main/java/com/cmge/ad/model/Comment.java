package com.cmge.ad.model;

/**
 * @desc	评论对象
 * @author	ljt
 * @time	2015-5-6 下午6:12:12
 */
public class Comment {
	
	private Integer id;
	
	// 类型 1:段子 2：图片
	private int type;
	
	// 点赞数
	private int zan;
	
	// 评论内容
	private String comment;
	
	// 用户Id
	private Integer userId;
	
	// 用户名  设备名称
	private String userName;
	
	// 评论时间
	private String createTime;

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getZan() {
		return zan;
	}

	public void setZan(int zan) {
		this.zan = zan;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
