package com.cmge.ad.model;

/**
 * @desc	菜单
 * @author	ljt
 * @time	2015-1-4 上午9:42:58
 */
public class Menu {
	
	private Integer id;
	
	private String menuName;
	
	private String menuCode;
	
	private Integer parentId;
	
	private String parentMenuName;
	
	// 1:没有子菜单的图文列表  2:带子菜单的图文列表 3:带子菜单的图片列表
	private int menuType;
	
	private String key;
	
	public String getParentMenuName() {
		return parentMenuName;
	}

	public void setParentMenuName(String parentMenuName) {
		this.parentMenuName = parentMenuName;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public int getMenuType() {
		return menuType;
	}

	public void setMenuType(int menuType) {
		this.menuType = menuType;
	}
}
