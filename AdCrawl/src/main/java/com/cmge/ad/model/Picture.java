package com.cmge.ad.model;

/**
 * @desc	图片
 * @author	ljt
 * @time	2014-12-30 下午7:48:30
 */
public class Picture {
	
	private Integer id;
	
	private Integer albumId;
	
	private String minImageUrl;
	
	private String maxImageUrl;
	
	private String desc;
	
	private int zan;
	
	// 源站
	private String source;
	
	private int width;
	
	private int height;
	
	private String locationUrl;
	
	private String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAlbumId() {
		return albumId;
	}

	public void setAlbumId(Integer albumId) {
		this.albumId = albumId;
	}

	public String getMinImageUrl() {
		return minImageUrl;
	}

	public void setMinImageUrl(String minImageUrl) {
		this.minImageUrl = minImageUrl;
	}

	public String getMaxImageUrl() {
		return maxImageUrl;
	}

	public void setMaxImageUrl(String maxImageUrl) {
		this.maxImageUrl = maxImageUrl;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getZan() {
		return zan;
	}

	public void setZan(int zan) {
		this.zan = zan;
	}

	@Override
	public String toString() {
		return "Picture [minImageUrl=" + minImageUrl + ", maxImageUrl=" + maxImageUrl + ", desc=" + desc + ", zan=" + zan + "]";
	}
}
