package com.cmge.ad.model;

/**
 * @desc	段子仓库
 * @author	ljt
 * @time	2015-1-27 下午4:54:15
 */
public class ArticleStore {
	
	private Integer id;
	
	// 唯一标识
	private String uniqueId;
	
	// 段子内容
	private String content;
	
	// 小图
	private String minImageUrl;
	
	// 大图
	private String maxImageUrl;
	
	// 段子类型   1：无图  2：有图
	private int type;
	
	// 状态  0：未同步  1：已同步 2：不可用
	private int statue;
	
	// 是否推荐 0：不是 1：是
	private int hot;
	
	// 抓取时间
	private String createTime;
	
	// 脑经急转弯答案
	private String answer;
	
	private Integer siteId;
	
	private Integer siteCategoryId;
	
	// 站点名称
	private String siteName;
	
	// 站点分类
	private String siteCategoryName;
	
	// 1:上一个 2：下一个
	private int orient;
	
	private int width;
	
	private int height;
	
	private Integer menuId;
	
	private String locationUrl;
	
	// 赞
	private int zan;
	
	public int getZan() {
		return zan;
	}

	public void setZan(int zan) {
		this.zan = zan;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getOrient() {
		return orient;
	}

	public void setOrient(int orient) {
		this.orient = orient;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public Integer getSiteCategoryId() {
		return siteCategoryId;
	}

	public void setSiteCategoryId(Integer siteCategoryId) {
		this.siteCategoryId = siteCategoryId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteCategoryName() {
		return siteCategoryName;
	}

	public void setSiteCategoryName(String siteCategoryName) {
		this.siteCategoryName = siteCategoryName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMinImageUrl() {
		return minImageUrl;
	}

	public void setMinImageUrl(String minImageUrl) {
		this.minImageUrl = minImageUrl;
	}

	public String getMaxImageUrl() {
		return maxImageUrl;
	}

	public void setMaxImageUrl(String maxImageUrl) {
		this.maxImageUrl = maxImageUrl;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getStatue() {
		return statue;
	}

	public void setStatue(int statue) {
		this.statue = statue;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getHot() {
		return hot;
	}

	public void setHot(int hot) {
		this.hot = hot;
	}
}
